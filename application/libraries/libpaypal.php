<?php
	class libpaypal{
		
		function __construct()
		{
		}
		
		function init($paypal)
		{
			$this->API_USERNAME = $paypal->paypal_api_username;
			$this->API_PASSWORD = $paypal->paypal_api_password;
			$this->API_SIGNATURE = $paypal->paypal_signature;
			$this->API_ENDPOINT = 'https://api-3t.sandbox.paypal.com/nvp';
			$this->USE_PROXY = FALSE;
			$this->PROXY_HOST = '127.0.0.1';
			$this->PROXY_PORT = '808';
			$this->PAYPAL_URL = 'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=';
			$this->VERSION = '65.1';
			$this->ACK_SUCCESS = 'SUCCESS';
			$this->ACK_SUCCESS_WITH_WARNING = 'SUCCESSWITHWARNING';
			$this->SUBJECT = '';
		}
		
		protected function nvpHeader()
		{
			$nvpHeaderStr = "";
			if(!empty($this->AUTH_MODE)) {
				$AuthMode = "AUTH_MODE"; 
			} 
			else {
	
				if((!empty($this->API_USERNAME)) && (!empty($this->API_PASSWORD)) && (!empty($this->API_SIGNATURE)) && (!empty($this->SUBJECT)))
				$AuthMode = "THIRDPARTY";
				
				else if((!empty($this->API_USERNAME)) && (!empty($this->API_PASSWORD)) && (!empty($this->API_SIGNATURE))) 
				$AuthMode = "3TOKEN";
				
				elseif (!empty($this->AUTH_TOKEN) && !empty($this->AUTH_SIGNATURE) && !empty($this->AUTH_TIMESTAMP))
				$AuthMode = "PERMISSION";
	
				elseif(!empty($this->SUBJECT))
				$AuthMode = "FIRSTPARTY";
			}
			switch($AuthMode) 
			{
				case "3TOKEN" : 
					$nvpHeaderStr = "&PWD=".urlencode($this->API_PASSWORD)."&USER=".urlencode($this->API_USERNAME)."&SIGNATURE=".urlencode($this->API_SIGNATURE);
				break;
				case "FIRSTPARTY" :
					$nvpHeaderStr = "&SUBJECT=".urlencode($this->SUBJECT);
				break;
				case "THIRDPARTY" :
					$nvpHeaderStr = "&PWD=".urlencode($this->API_PASSWORD)."&USER=".urlencode($this->API_USERNAME)."&SIGNATURE=".urlencode($this->API_SIGNATURE)."&SUBJECT=".urlencode($this->SUBJECT);
				break;		
				case "PERMISSION" :
					$nvpHeaderStr = $this->formAutorization($this->AUTH_TOKEN,$this->AUTH_SIGNATURE,$this->AUTH_TIMESTAMP);
				break;
			}
			return $nvpHeaderStr;
		}
		
		function hash_call($methodName,$nvpStr)
		{		
			// form header string
			$nvpheader=$this->nvpHeader();
			//setting the curl parameters.
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$this->API_ENDPOINT);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);

			//turning off the server and peer verification(TrustManager Concept).
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_POST, 1);
	
			//in case of permission APIs send headers as HTTPheders
			if(!empty($this->AUTH_TOKEN) && !empty($this->AUTH_SIGNATURE) && !empty($this->AUTH_TIMESTAMP))
			{
				$headers_array[] = "X-PP-AUTHORIZATION: ".$nvpheader;
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_array);
				curl_setopt($ch, CURLOPT_HEADER, false);
			}
			else 
			{
				$nvpStr=$nvpheader.$nvpStr;
			}
			//if USE_PROXY constant set to TRUE in Constants.php, then only proxy will be enabled.
			//Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php 
			if($this->USE_PROXY)
			curl_setopt ($ch, CURLOPT_PROXY, $this->PROXY_HOST.":".$this->PROXY_PORT); 

			//check if version is included in $nvpStr else include the version.
			if(strlen(str_replace('VERSION=', '', strtoupper($nvpStr))) == strlen($nvpStr)) {
				$nvpStr = "&VERSION=" . urlencode($this->VERSION) . $nvpStr;	
			}
	
			$nvpreq="METHOD=".urlencode($methodName).$nvpStr;
		
			//setting the nvpreq as POST FIELD to curl
			curl_setopt($ch,CURLOPT_POSTFIELDS,$nvpreq);

			//getting response from server
			$response = curl_exec($ch);

			//convrting NVPResponse to an Associative Array
			$nvpResArray=$this->deformatNVP($response);
			$nvpReqArray=$this->deformatNVP($nvpreq);
			$_SESSION['nvpReqArray']=$nvpReqArray;
	
			if (curl_errno($ch)) {
				// moving to display page to display curl errors
				$_SESSION['curl_error_no']=curl_errno($ch) ;
				$_SESSION['curl_error_msg']=curl_error($ch);
				$location = base_url("inicio/paypal_error");
				header("Location: $location");
			} else {
				//closing the curl
				curl_close($ch);
			}

			return $nvpResArray;
		}

		function deformatNVP($nvpstr)
		{

			$intial=0;
			$nvpArray = array();
			while(strlen($nvpstr))
			{
				//postion of Key
				$keypos= strpos($nvpstr,'=');
				//position of value
				$valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);
				/*getting the Key and Value values and storing in a Associative Array*/
				$keyval=substr($nvpstr,$intial,$keypos);
				$valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
				//decoding the respose
				$nvpArray[urldecode($keyval)] =urldecode( $valval);
				$nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
			}
			return $nvpArray;
		}
		
		function formAutorization($auth_token,$auth_signature,$auth_timestamp)
		{
			$authString="token=".$auth_token.",signature=".$auth_signature.",timestamp=".$auth_timestamp ;
			return $authString;
		}		
	}
?>