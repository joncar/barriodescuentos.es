<div id="alert-boletin" class="alert alert-info">
    <h1>¿A quienes le enviamos el boletin?</h1>
    <form onsubmit="return send()"><br/>
        
        Remitente: <input type="text" name="email" id="email" value="info@barriodescuentos.es"><br/>
        Destinatarios:<br/>
        <input type="radio" name="dest" checked id="dest" value="Todos"> Todos los registrados<br/>
        <input type="radio" name="dest" id="dest2" value="Inactivos"> Todos los usuarios inactivos<br/><br/>
        <input type="submit" name="boletin" id="boletin" value="Enviar" class="btn btn-success">
    </form>
</div>
<script>
    function send()
    {
       
        
        if($("#email").val()!='')
        {
            var dest = $("#dest").prop('checked')?$("#dest").val():$("#dest2").val();
            
            ajax('id=<?= $id ?>&email='+$("#email").val()+'&dest='+dest,true,"#alert-boletin",undefined,'<?= base_url('inicio/sendBol') ?>')
        }
        return false;
    }
</script>
