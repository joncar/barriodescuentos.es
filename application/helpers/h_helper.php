<?php function to_url($nombre){
    $nombre = str_ireplace(" ","-",$nombre); 
    $nombre = str_replace(
        array("\\", "¨", "º", "~",
             "#", "@", "|", "!", "\"",
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "`", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             ".", " "),
        '',
        $nombre
    );
    
    return $nombre;
} 
    function showListProduct($list,$texto = '')
    {
        $x = '<div class="megafolio-container" >';
	$p = 0;	
        foreach($list->result() as $row)
           {
               $nombre = strlen($row->nombre)>=50?mb_substr($row->nombre,0,50)."...":$row->nombre;
               $p++;
                 $x.= '
                        <div class="mega-entry cat-all cat-'.$row->prodCat.' well" id="mega-entry-10" data-src="'.base_url($row->url).'" data-width="504" data-height="400">
                        <div class="mega-hover">
                            <div class="mega-hovertitle">'. $nombre.'<div class="mega-hoversubtitle">
                                <p>Válido hasta: '.date("d/m/Y H:i:s",strtotime($row->date_final)).' </p>
                                <p><a href="'.site_url("show/v/".$row->idCampana."/".to_url($row->nombre)).'"><button class="btn btn-primary"><i class="icon-white icon-plus-sign"></i> Ver más</button></a></p>
                                 </div></div>
                                
                            </div>
                        </div>
                    ';
            }
            $x.= '</div>';
		if($list->num_rows==0)
		$x = '<h4>Lo sentimos por ahora no tenemos productos para mostrar, Se tu el primero en publicar alguno	</h4>';
		else
		{
			if(empty($texto))
			$texto = $x;
			else
			$texto = str_replace('[list]',$x,$texto);
			return $texto;
		}
		return $x;
    }
	
	function noticias($page,$texto = '')
	{
		$t = '';
                if($page->num_rows==0)
                    $t.= '<article class="container well">
		
           
		Por ahora no tenemos noticias para mostrar
          <div align="right" style="width:100%; margin-top:30px">
			<span class="label">Fecha: '.date('d/m/Y').'</span>
			</div>	
        </article>';
		foreach($page->result() as $page){
		$t.= '<article class="container well">
		<h1>'.$page->titulo.'</h1>
           
		'.$page->texto.'
          <div align="right" style="width:100%; margin-top:30px">
			<span class="label">Fecha: '.date('d/m/Y',strtotime($page->fecha)).'</span>
			</div>	
        </article>';
		}
		if(empty($texto))
		$texto = $t;
		else
		$texto = str_replace('[noticias]',$t,$texto);
		return $texto;
	}
	
	function offerday($offerday,$texto='')
	{
		$t = '
                <article class="span12" style=" margin-left:0px; background:url('.base_url('assets/uploads/'.$offerday->url).'); background-position:center; background-size:100%; height: 400px;">
                <div>
                <div class="pull-right" style="text-align:justify; background: none repeat scroll 0 0 rgba(0, 0, 0, 0.7); color: #FFFFFF; height: 360px; padding: 20px; text-align: center; width: 320px;">
                <h4 align="justify">'.$offerday->nombre.'</h4>
		<p align="justify">'.$offerday->descripcion.'</p>
		<p>Finaliza en: <span class="label label-info">'.$offerday->date_final.'</span></p>                
                <p align="center">
                <a href="'.site_url("show/v/".$offerday->idCampana."/".to_url($offerday->nombre)).'">
                    <button class="btn btn-primary btn-large"><i class="icon-white icon-plus-sign"></i> Ver más</button>
                </a></p>
                </div>
                </div>
                </article>';
		
		if(empty($texto))
		$texto = $t;
		else
		$texto = str_replace('[offerday]',$t,$texto);
		return $texto;
	}
	
	function cut_number($val)
	{
		$num = strlen($val)-4;
		$val = substr($val,0,4);
		for($i=0;$i<$num;$i++)$val.='x';
		return $val;
	}
	
	function  correo($email = 'joncar.c@gmail.com',$titulo = '',$msj = '',$from='Barriodescuentos<administracion@barriodescuentos.es>')
        {            
            $sfrom=$from; //cuenta que envia
            $sdestinatario=$email; //cuenta destino
            $ssubject=$titulo; //subject
            $shtml=$msj; //mensaje
            $shtml.='';
            $sheader="From:".$sfrom."\nReply-To:".$sfrom."\n";
            $sheader=$sheader."X-Mailer:PHP/".phpversion()."\n";
            $sheader=$sheader."Mime-Version: 1.0\n";
            $sheader=$sheader."Content-Type: text/html; charset:utf-8";
            mail($sdestinatario,$ssubject,$shtml,$sheader);
        }
	
	
	function mail_activate($email,$name,$codigo_activacion)
	{
		$template = template_newusr_mail($name,$email,$codigo_activacion);
		correo($email,'Hola, '.$name.' gracias por registrarte, activa tu cuenta',$template);
	}
	
	function mail_recover($email,$codigo)
	{
		$template = template_recover_mail($email,$codigo);
		correo($email,'Solicitud de restablecimiento de contraseña',$template);
	}
	
	/********* templates mail ***********/
	function template_newusr_mail($name,$email,$codigo)
        {
	$ruta = base_url('registrar/activate/'.urlencode($email).'/'.$codigo);
	return '<div style="background:#77b7c8; color:#004455; width:600px; height:auto; border-radius:1em; -moz-border-radius:1em; border:1px solid #004455; padding:10px;">
	<div style="background:white; border-radius:1em; -moz-border-radius:1em; padding:10px;">
	<h1 style="text-align:center">Hola '.$name.', gracias por registrarte en barriodescuentos.com</h1>
	<h3>Debes activar tu cuenta para empezar a publicar cupones</h3>
	<b>Pulsa en el enlace para activar tu cuenta <br/><a href="'.$ruta.'">'.$ruta.'</a></b>
	<ul>
		<li><a href="'.site_url().'">Publicar ofertas</a></li>
		<li><a href="'.site_url().'">Conoce las empresas registrados en la web</a></li>
		<li><a href="'.site_url().'">Revisa tus ofertas publicadas, modificalas o eliminalas</a></li>
		<li><a href="'.site_url('pages/2013/como-funciona.html').'">Conoce como funciona barriodescuentos</a></li>
	</ul>
		<p align="center"><a href="'.site_url('pages/2013/terminos-y-condiciones.html').'">Politicas y condiciones de uso</a> | <a href="'.site_url('contactar').'">Contactar</a></p>
	</div>
</div>';
        }
		
		
	function template_recover_mail($email,$codigo)
    {
	$ruta = base_url('registrar/recover/'.urlencode($email).'/'.$codigo);
	return '<div style="background:#77b7c8; color:#004455; width:600px; height:auto; border-radius:1em; -moz-border-radius:1em; border:1px solid #004455; padding:10px;">
	<div style="background:white; border-radius:1em; -moz-border-radius:1em; padding:10px;">
	<h1 style="text-align:center">Haz solicitado tu contraseña</h1>
	<b>Pulsa en el enlace para restablecer tu contraseña <br/><a href="'.$ruta.'">'.$ruta.'</a></b>
	</div>
</div>';
    }
?>