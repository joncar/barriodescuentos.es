<?php
	require_once("panel.php");
	class Empresa extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('empresa');
                        
                        if($_SESSION['type']!='admin')
			$crud->where('empresa.user',$_SESSION['user']);
			$crud->set_relation('ciudad','ciudades','nombre');
			//$crud->set_relation('pueblo','pueblos','nombre');
                        $crud->set_relation('categoria','categorias','nombre');
			$crud->set_subject('Empresas');
                        $crud->field_type('user', 'invisible');
                        $crud->field_type('status', 'invisible');
                        $crud->callback_field('categoria',array($this,'catfield'));
                        $crud->required_fields('nombre','ciudad','direccion','descripcion','categoria','horario'); 
			if($_SESSION['type']!='admin')
                        $crud->unset_columns('paypal_api_username','paypal_api_password','paypal_signature');
                        else{
                        $crud->set_relation('user','user','nombre');
                        $crud->unset_columns('paypal_api_username','paypal_api_password','paypal_signature','user');
                        }
			//Column
                        $crud->unset_fields('paypal_api_username','paypal_api_password','paypal_signature','pueblo');
                        if($_SESSION['type']=='admin')
                        $crud->unset_columns('paypal_api_username','paypal_api_password','paypal_signature','user','pueblo');
                        else
                        $crud->columns('nombre','categoria','status');
                        $crud->set_field_upload('logo','assets/uploads/maps');
			$crud->display_as("ciudad","Localidad");
                        $crud->display_as("paypal","Cuenta paypal");
                        $crud->display_as("descripcion","Descripcion de la empresa");
                        $crud->display_as("horario","Horario de Atención");
			$crud->callback_column('logo',array($this,'img'));
			$crud->callback_column('status',array($this,'status'));
			$crud->callback_field('pueblo',array($this,'pueblo'));
                        $crud->callback_field('ubicacion',array($this,'map'));
			$crud->callback_before_insert(array($this,'binsertion'));
                        $crud->callback_before_update(array($this,'binsertion'));
			$crud->unset_delete();
			//Validations
			$crud->set_rules('nombre','Nombre','required');
                        $crud->set_rules('descripcion','Descripcion','required|max_length[255]');
			//$crud->set_rules('pueblo','Pueblo','required');
			$crud->set_rules('ciudad','Ciudad','required');
			$crud->set_rules('direccion','Direccion','required');
			$crud->set_rules('telefono','Telefono','required');
                        $crud->add_action('QR', '',base_url('empresa/qr')."/",'icon icon-barcode');
			$output = $crud->render();
			$this->dir($output,'panel_empresa');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	
			
		}
                
                function map()
                {
                    return $this->load->view('mapform',null,TRUE);
                }
		
		function get_options($id)
		{
			$sel = '';
			if(empty($id))
			$sel.= '<option value="">Selecciona una ciudad</option>';
			else
			foreach($this->db->get_where('pueblos',array('id_ciudad'=>$id))->result() as $row)
			$sel.= '<option value="'.$row->id.'">'.$row->nombre.'</option>';
			return $sel;
		}
		
		function pueblo($id = '')
		{
			$sel = '<select name="pueblo" id="field-pueblo">';
			$sel.= $this->get_options($id);
			$sel.= '</select>';
			return $sel;
		}
		
		function AjaxPueblo()
		{
			echo $this->pueblo($this->input->post('id'));
		}
		
		function status($val,$row)
		{
			switch($val)
			{
				case '0':$x = 'Bloqueada'; break;
				case '1':$x = 'Activa'; break;
				case '2':$x = 'Por validar'; break;
			}
				$option = $_SESSION['type']=='admin'?' <a href="javascript:change(\''.$row->id.'\',\'activate\')" class="icon icon-ok" title="Activar"></a> <a href="javascript:change(\''.$row->id.'\',\'block\')" class="icon icon-ban-circle" title="Bloquear"></a>':'';
				return '<span id="s'.$row->id.'">'.$x.$option.'</span>';
		}
		
		function binsertion($post_array)
		{
			$post_array = $this->adduser($post_array);
			$post_array['status'] = 2;
                        
                        //Agregar categoria
                        if($this->db->get_where('categorias',array('nombre'=>$post_array['categoria']))->num_rows==0)
                        {
                            $this->db->insert('categorias',array('nombre'=>$post_array['categoria']));
                            $post_array['categoria'] = $this->db->get_where('categorias',array('nombre'=>$post_array['categoria']))->row()->id;
                        }
                        else
                            $post_array['categoria'] = $this->db->get_where('categorias',array('nombre'=>$post_array['categoria']))->row()->id;
                        
									//Se valida si la ciudad esta activa y si no pues se activa
			$ciudad = $this->db->get_where('ciudades',array('id'=>$post_array['ciudad']))->row()->visible;
			if($ciudad==0)
			$this->db->update('ciudades',array('visible'=>'1'),array('id'=>$post_array['ciudad']));
			return $post_array;
		}
		
                function img($val,$row)
                {
                    return '<img src="'.base_url('assets/uploads/maps/'.$val).'" width="40" height="40">';
                }
		
		function change($emp,$val)
		{
			$status = $val == 'activate'?'1':'0';
			$this->db->where('id',$emp);
			$this->db->update('empresa',array('status'=>$status));
			echo $this->status($status,(object)array('id'=>$emp));
		}
                
                function catfield($val)
                {
                    $y = '';
                    foreach($this->db->get('categorias')->result() as $x)
                    $y.= "'".$x->nombre."',";
                    return 
                    '<script src="'.base_url('assets/typehead.js').'"></script>
                    <script>
                    $(document).ready(function(){$("#field-categoria").typeahead({name: "categoria",local: ['.$y.']})});    
                    </script>'.
                    form_input('categoria','','id="field-categoria" placeholder="Escriba una categoria"');
                }
                
                function qr($id)
                {
                    $empresa = $this->db->get_where('empresa',array('id'=>$id));
                    if($empresa->num_rows>0)
                    {
                         
                        $params['data'] = " Pulsa en el enlace para ver la empresa. ".site_url("show/e/".str_replace("+","-",urlencode($empresa->row('nombre')))."-".$empresa->row()->id.".html");
                        $params['level'] = 'M';
                        $params['size'] = 3;
                        $params['savename'] = FCPATH.'tes.png';
                        $this->ciqrcode->generate($params);
                        
                        header('Content-Type: image/png');
                        header('Content-Disposition: attachment; filename="Codigo Qr de '.$empresa->row()->nombre); //<<< Note the " " surrounding the file name
                        
                        $imagen = imagecreatefrompng(base_url('img/cartel.png'));
                        $im     = imagecreatefrompng(base_url()."tes.png");
                        imagecopy($imagen,$im, 100, 340, 0,0, 130,130);
                        imagepng($imagen);
                        
                        imagedestroy($imagen);
                        imagedestroy($im);
                    }
                    else
                    echo "Empresa no encontrada";
                }
	}
?>
