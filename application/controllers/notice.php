<?php
	require_once("inicio.php");
	class Notice extends Inicio{
		function __construct()
		{
			parent::__construct();
		}
		
		function index()
		{
			$page = $this->main->getNotice();
			$this->load->view('template',array('view'=>'noticias','page'=>$page));
		}
	}
?>
