<?php
	require_once('inicio.php');
    require_once("registrar.php");
	class Panel extends Inicio{

		function __construct()
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('grocery_CRUD'); 
			$this->load->library('form_validation'); 
			$this->load->model('user');

			$this->js = array(
				base_url('assets/grocery_crud/js/jquery-1.8.2.min.js'),
				base_url('assets/grocery_crud/themes/flexigrid/js/cookies.js'),
				
				base_url('assets/grocery_crud/themes/flexigrid/js/jquery.form.js'),
				base_url('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js'),
				base_url('assets/grocery_crud/themes/flexigrid/js/jquery.printElement.min.js'),
				base_url('assets/grocery_crud/js/jquery_plugins/jquery.fancybox.pack.js'),
				base_url('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js')
			);
			$this->css = array(base_url('assets/grocery_crud/themes/flexigrid/css/flexigrid.css'),
				     base_url('assets/grocery_crud/js/jquery_plugins/fancybox/jquery.fancybox.css')
			);

			ini_set('date.timezone', 'America/Caracas');
		}
		
		function index()
		{
                                            
			if(!isset($_SESSION['user']))
			$this->login();
			else                            
                        {
                            $view = 'panel_index';
                            $this->dir((object)array('output' => '' ,
                            'js_files' => $this->js , 
                            'css_files' => $this->css,
                            'view'=>$view));
                        }
		}
		
		
		function login()
		{
			$x = new Registrar();
            $x->index();
		}

		function desconectar()
		{
			session_unset();
			header("Location:inicio");
			exit();
		}

		function add_dropdown($name,$data,$extra = array('class'=>''))
		{

 		 $html = '<link type="text/css" rel="stylesheet" href="'.base_url().'/assets/grocery_crud/css/jquery_plugins/chosen/chosen.css" />';
		 $html .= '<script src="'.base_url().'/assets/grocery_crud/js/jquery_plugins/jquery.chosen.min.js"></script>';
		 $html .= '<script src="'.base_url().'/assets/grocery_crud/js/jquery_plugins/config/jquery.chosen.config.js"></script>'; 
		 $html.= ''.form_dropdown($name,$data,'','class="chosen-select '.$extra['class'].'" data-placeholder="Selecciona '.$name.'" style="width: 200px;"').'';
		 return $html; 
		}

		public function price($val)
		{
		return $this->main->price($val);
		}
                
                public function adduser($post_array)
                {
                    $post_array['user'] = $_SESSION['user'];
                    return $post_array;
                }
                
                public function delete_user()
                {
                    if(!empty($_SESSION['user']))
                    {
                        $this->db->where('id',$_SESSION['user']);
                        $this->db->update('user',array('status'=>-1));
                        //$this->unlog();
                        header("Location:".site_url('inicio/adios'));
                    }
                    $this->loadView(array('view'=>'404'));
                }

	}
?>
