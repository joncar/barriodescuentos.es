<?php
	require_once("panel.php");
	class Noticias extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
			$crud = new grocery_CRUD();
			$crud->set_theme('flexigrid');
			$crud->set_table('noticias');
                	$crud->set_subject('Noticias');   
                        $crud->unset_fields('vistas');
			$crud->required_fields('titulo','texto','fecha','permiso');
                        $crud->field_type('permiso','set',array('registrados','visitantes'));
			$output = $crud->render();
			$this->dir($output,'panel_empresa');
		}
	}
?>
