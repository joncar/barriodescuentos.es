<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_start();
class Inicio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */   
	var $rules_admin = array('all');
	var $rules_user = array('show/e','inicio','inicio/adios','inicio/edit_user_form','tarjetas','pagos','show/v','show/cat','panel','empresa','compras','inicio/hucha','edit_user_form','notice','404','panel/delete_user');
	var $rules_guest = array('show/e','registrar','inicio/adios','registrar/forget','registrar/recover','inicio','show/v','show/cat','404');
	var $no_autorize = 'conectar';
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('url_extend');
		$this->load->helper('form');
		$this->load->helper('h');
		$this->load->database();
		$this->load->model('user');
                $this->load->model('main');
		$this->load->library('form_validation');
		$this->load->library('libpaypal');
                $this->load->library('ciqrcode');
		$this->libpaypal->init($this->db->get('ajustes')->row());
		$this->main->uploadCupons();
		if(!empty($_SESSION['user'])){
			if($this->db->get_where('empresa',array('user'=>$_SESSION['user']))->num_rows>0);
			$this->rules_user = array_merge(array('productos','campains'),$this->rules_user);
		}
	}
	public function index($url = '')
	{
          
          $list = $this->main->getList();
          $offerday = $this->main->getOfferDay();
	  if(($list || $offerday) && !$this->main->getPage($url))
	  $this->loadView(array('view'=>'conectar'));
          //$this->loadView(array('view'=>'main','list'=>$list,'offerday'=>$offerday));
	  else if($page = $this->main->getPage($url))
	  {
              if(!empty($_POST['sendContact']))
              $this->sendContact();
              else
              $this->loadView(array('view'=>'page','page'=>$page));
          }
	  
          else
	  $this->loadView(array('view'=>'404'));
	}
        
        function sendContact()
        {
            
            $this->form_validation->set_rules('name','Nombre','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('msj','Mensaje','required');
            if($this->form_validation->run())
            {
                $nombre = $this->input->post('name');
                $msj = '';
                foreach($_POST as $x=>$v)
                $msj.= "<b>".$x.": </b>".$v."<br>";
               
                correo('contacto@barriodescuentos.com',$nombre." Intenta contactarlos",$msj);
                correo('joncar.c@gmail.com',$nombre." Intenta contactarlos",$msj);
                $this->loadView(array('view'=>'page','page'=>$this->main->getPage('contactenos'),'msj'=>$this->success('Mensaje enviado con exito')));
            }
            else
            $this->loadView(array('view'=>'page','page'=>$this->main->getPage('contactenos'),'msj'=>$this->error($this->form_validation->string_error())));
        }

	public function success($msj)
	{
		return '<div class="alert alert-success">'.$msj.'</div>';
	}
        
        function oto($date_fin)
        {   
            $date1=date("Y-m-d H:i:s");
            $date2=$date_fin;
            $diff = strtotime($date2) - strtotime($date1); 
            $years   = floor($diff / (365*60*60*24)); 
            $months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
            $days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            $hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60)); 
            $minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 
            $seconds = ($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60);
            $s = $diff;
            return $s;
        }

	public function error($msj)
	{
		return '<div class="alert alert-danger">'.$msj.'</div>';
	}
	
	public function ciudad($ciudad)
	{
		$_SESSION['ciudad'] = $ciudad;
		header("Location:".base_url());
		
	}
        


	public function login()
	{
		if(!$this->user->log)
		{	
			if(!empty($_POST['user']) && !empty($_POST['pass'])){
				$this->db->where('email',$this->input->post('user'));
				$r = $this->db->get('user');
				if($this->user->login($this->input->post('user',TRUE),$this->input->post('pass',TRUE)))
				{
						if($r->num_rows>0 && $r->row()->status==1)
						{
                                        if(empty($_POST['redirect']))
                                        echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url('panel').'"</script>');
                                        else
                                        echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
						}
						else echo $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
				}
				else echo $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
			}
			else
				echo $this->error('Debe completar todos los campos antes de continuar');
		}
	}

	public function unlog()
	{
		$this->user->unlog();
                header("Location:".site_url());
	}
        
        public function sell($id_campana)
        {
            if(!empty($_SESSION['user']))
            {
            $data = array('id_campana'=>$id_campana,'user'=>$_SESSION['user'],'Fecha'=>date("Y-m-d H:i:s"));
            $this->db->insert('ventas',$data);
            echo $this->success("Producto agregado a las capturas.");
            }
            else
            {
                $this->load->view('acceso_directo',array('redirect'=>base_url('show/v/'.$id_campana),'msj'=>''));
            }
        }
        
        public function follow()
        {
            if(!empty($_SESSION['user'])){
            $this->form_validation->set_rules('e','','required|integer');
            if($this->form_validation->run())
            {
                $this->db->insert('follows',array('user'=>$_SESSION['user'],'empresa'=>$this->input->post('e',TRUE)));
                echo 'Seguido con exito';
            }
            else
                echo $this->form_validation->error_string();
            }
        }
        
        public function loadView($param = array('view'=>'main'))
        {
			if($this->valid_rules())
            $this->load->view('template',$param);
			else
			header("Location:".site_url($no_autorize));
        }
		
		public function loadViewAjax($view,$data)
        {
			if($this->valid_rules()){
                        if(!empty($data))$data = array('id'=>$data);
                         $this->load->view($view,$data);
			}
			else
			header("Location:".site_url($no_autorize));
        }
		
		public function dir($output = null,$link = '')
		{
			if($this->valid_rules()){
			if($link!="")$output->view = $link;
			$this->load->view('template_panel',$output);}
			else
			header("Location:".site_url($no_autorize));
		}
		
	public function edit_user_form()
	{
		if(!empty($_POST))
		{
			$err = 0;
			if(!empty($_POST['nombre'])){
			$_SESSION['nombre'] = $this->input->post('nombre',TRUE);
			$data['nombre'] = $this->input->post('nombre',TRUE);
			}
			else
			$err = 1;
			if(!empty($_POST['telefono']))
			$data['telefono'] = $this->input->post('telefono',TRUE);
			if(!empty($_POST['direccion']))
			$data['direccion'] = $this->input->post('direccion',TRUE);
			if(!empty($_POST['pass']))
			{
				if($_POST['pass'] == $_POST['pass2'] && strlen($_POST['pass'])>=8)
				$data['pass'] = md5($this->input->post('pass',TRUE));
				else
				$err = 1;
			}
			
			if($err==0)
			{
				$this->user->edit($data);
				echo $this->success('Datos Almacenados correctamente.');
			}
			else
				echo $this->error('Las contraseñas deben ser iguales y se debe elegir un nombre');
		}
		else
		{
		$datos = $this->db->get_where('user',array('id'=>$_SESSION['user']))->row();
		$this->loadView(array('view'=>'useredit','datos'=>$datos));
		}
	}
	
	public function hucha()
	{
		$this->loadView(array('view'=>'hucha','hucha'=>$this->main->getHucha()));
	}
	
	public function valid_rules() //Methodo para validar si el acceso es autorizado
	{
		if(!empty($_SESSION['user']))
			$type = $_SESSION['type'];
		else
			$type = "guest";
		
		switch($type)
		{
			case 'admin': $rules = $this->rules_admin; break;
			case 'user': $rules = $this->rules_user; break;
			case 'guest': $rules = $this->rules_guest; break;
		}
		$request = ($this->router->fetch_method()!='index')?$this->router->fetch_class().'/'.$this->router->fetch_method():$this->router->fetch_class();
		if(in_array($request,$rules) || in_array('all',$rules)){
			return true;
        }
		
		else
		return false;
	}
		
	public function subscribirform()
	{
		echo '
			<div  style="text-align:center">
<h4 class="alert alert-success">Subscribete</h4>			
			<input type="text" placeholder="Nombre"><br>
			<input type="email" placeholder="Email"><br/>
                        <button type="submit" class="btn btn-success">Guardar</button>
</div>';
	}
	
	public function sendBol()
	{
		if(!empty($_POST['email']) && !empty($_POST['dest']) && !empty($_POST['id']))
        {
                $this->form_validation->set_rules('id','Boletin','required|numeric');
                $this->form_validation->set_rules('email','Email','required|valid_email');
                $this->form_validation->set_rules('dest','Destinatario','required');
                if($this->form_validation->run())
                {
                if($this->main->sendBol($this->input->post('id',TRUE),$this->input->post('email',TRUE),$this->input->post('dest',TRUE)))
                    echo $this->success('Email registrado satisfactoriamente, gracias por registrarse a barriodescuentos.com.ve');
                else
                    echo $this->error('el email ya se encuentra registrado por favor intenté nuevamente');
                }
                else
                    echo $this->error($this->form_validation->error_string());
                $this->loadViewAjax('sendBoletin',$_POST['id']);
        }
	}
        
        public function adios()
        {
            $this->loadView(array('view'=>'adios'));
            $this->user->unlog();
        }
        
        function politicas()
        {
            $_SESSION['politicas'] = 'aceptar';
            echo 'success';
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */