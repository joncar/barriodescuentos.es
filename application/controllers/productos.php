<?php
	require_once("panel.php");
	require_once("images.php");
	class Productos extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();
                        $crud->unset_jquery();
			$crud->set_theme('flexigrid');
			$crud->set_table('productos');
			$crud->set_subject('Productos');
                        $crud->set_relation('id_categoria','categorias','Nombre');
                        $crud->set_relation('user','user','nombre');
						
			$val = $_SESSION['type']=='admin'?null:array('empresa.user'=>$_SESSION['user']);
			$crud->set_relation('empresa','empresa','nombre',$val);
						
                        if($_SESSION['type']!='admin')$crud->where('productos.user',$_SESSION['user']);
			$crud->columns('nombre','descripcion','precio','fotos','empresa');
                        if($_SESSION['type']=='admin')
                        $crud->columns('nombre','descripcion','precio','fotos','user','empresa');
                        $crud->field_type('user','invisible');
                        $crud->callback_before_insert(array($this,'binsertion'));
                        $crud->display_as('id_categoria','Categoria');
                        $crud->display_as('precio','Precio real sin descuento');
			//Column
			$crud->callback_column('fotos',array($this,'listfotos'));
			$crud->callback_column('precio',array($this,'price'));
                        $crud->required_fields('nombre','descripcion','empresa','categoria','precio');
                        $crud->set_rules("nombre", "Nombre", "required");
                        $crud->set_rules("descripcion", "Descripcion", "required");
                        $crud->set_rules("precio","Precio","required|numeric");
                        
			$output = $crud->render();
			$this->dir($output,'panel_productos');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	
			
		}
		
		function binsertion($post)
		{
			$post = $this->adduser($post);
			$this->db->where('id',$post['empresa']);
			$r = $this->db->get('empresa')->row()->status;
			if($r!=1)
			return false;
			else
			return $post;
		}

		public function listfotos($val,$row)
		{
			$this->db->where('id_producto',$row->id);
                        $this->db->where('user',$_SESSION['user']);
			$this->db->order_by('priority');
			$r = $this->db->get('images');
			$x = '';
			foreach($r->result() as $ro)
			{
				$x.= '<img src="'.base_url('assets/uploads/'.$ro->url).'" width="20"> ';
			}
			
			return $x.' <a href="javascript:picture_manager(\''.$row->id.'\')"><i class="icon icon-plus-sign"></i></a>';
		}
		
		
	}
?>
