<?php
	require_once("panel.php");
	class Categorias extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('categorias');
			$crud->set_subject('Categorias registradas');
                        $crud->callback_before_delete(array($this,'bdelete'));
			$output = $crud->render();
			$this->dir($output,'usuarios');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
		}
                
                function bdelete($post)
                {
                    $this->db->where('id_categoria',$post);
                    $this->db->update('productos',array('id_categoria'=>1));
                    $this->db->where('categoria',$post);
                    $this->db->update('empresa',array('categoria'=>1));
                    return true;
                }
		
		
	}
?>
