<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('inicio.php');
class Conectar extends Inicio {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $msj = '';
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->database();
		if(!empty($_POST['reg']))
			$this->msj = $this->reg();
	}
	
	public function index()
	{
            if(!empty($_SESSION['user']))
                header("Location:".base_url('panel'));
            else
		$this->load->view('template',array('view'=>'conectar'));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */