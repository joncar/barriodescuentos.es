<?php
	require_once("panel.php");
	class Paginas extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();
			$crud->set_theme('flexigrid');
			$crud->set_table('paginas');
			$crud->set_subject('Paginas registradas');
			$crud->display_as('nombre','url');
			$crud->required_fields('titulo','texto');
			$crud->change_field_type('nombre','invisible');
			$crud->unset_add();
			$crud->unset_delete();
			$output = $crud->render();
			$this->dir($output,'usuarios');
                    }catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	
			
		}
		
		
	}
?>
