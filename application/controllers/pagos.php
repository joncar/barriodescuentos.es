<?php
	require_once("panel.php");
	class Pagos extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('pagos');
			$crud->set_relation('vendedor','user','nombre');
			$crud->set_subject('Pagos del sistema');
			$crud->unset_add();
			$crud->unset_columns('comprador');
            $crud->unset_delete();
			
			if($_SESSION['type']!='admin')
			$crud->where('vendedor',$_SESSION['user']);
			
			$output = $crud->render();
			$this->dir($output,'usuarios');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	
			
		}
		
		
	}
?>
