<?php
	require_once("inicio.php");
	require_once("application/libraries/libpaypal.php");
	class Paypal extends Inicio{
		function __construct()
		{
			parent::__construct();   
		}
		
		function clear()
		{
			$user = $_SESSION['user'];
            $type = $_SESSION['type'];
            $nombre = $_SESSION['nombre'];
			$apellido = $_SESSION['apellido'];
			session_unset();
			$_SESSION['user'] = $user;
			$_SESSION['type'] = $type;
			$_SESSION['nombre'] = $nombre;
			$_SESSION['apellido'] = $apellido;
		}
		
		function recibir_pago()
		{
			$this->form_validation->set_rules('creditCardType','Tipo de tarjeta de credito','required');
			$this->form_validation->set_rules('creditCardNumber','Numero de tarjeta de credito','required|integer');
			$this->form_validation->set_rules('expDateMonth','Mes de vencimiento','required|integer');
			$this->form_validation->set_rules('expDateYear','Año de vencimiento','required|integer');
			$this->form_validation->set_rules('cvv2Number','Numero de verificacion','required|integer|max_length[3]|min_length[3]');
			
			$tarjeta = $this->db->get_where('tarjetas',array('id'=>$this->input->post('tipopago',TRUE),'user'=>$_SESSION['user']));
			if($this->form_validation->run() || $tarjeta->num_rows>0)
			{
				$this->clear();
				$paymentType ='Sale';
				$firstName =urlencode($_SESSION['nombre']);
				$lastName =urlencode($_SESSION['apellido']);
				/**
				* Get required parameters from the web form for the request
				*/
				if($tarjeta->num_rows==0){
				$creditCardType =urlencode( $this->input->post('creditCardType',TRUE));
				$creditCardNumber = urlencode($this->input->post('creditCardNumber',TRUE));
				$expDateMonth =urlencode( $this->input->post('expDateMonth',TRUE));
				$padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
				$expDateYear =urlencode( $this->input->post('expDateYear',TRUE));
				$cvv2Number = urlencode($this->input->post('cvv2Number',TRUE));
				}
				else
				{
				$creditCardType =urlencode($tarjeta->row()->tarjeta);
				$creditCardNumber = urlencode($tarjeta->row()->numero);
				$expDateMonth =urlencode($tarjeta->row()->expmes);
				$padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
				$expDateYear =urlencode($tarjeta->row()->expano);
				$cvv2Number = urlencode($tarjeta->row()->verificacion);
				}
				
				
				$compra = $this->main->getCompra();
				//$currencyCode=urlencode($_POST['currency']);
				$currencyCode=$this->main->price_avr();
				/* Construct the request string that will be sent to PayPal.
				The variable $nvpstr contains all the variables and is a
				name value pair string with & as a delimiter */
				if($compra->num_rows>0)
				{
				$val = 0;
				$comisionTotal = 0;
				/*Pago a los vendedores*/
				foreach($compra->result() as $r)
				{
					if($val == 0){
					$porcentaje = $this->db->get('ajustes')->row()->porcentaje/100;
					$amount = $r->precio-($r->precio*($r->descuento/100));
					$comision = $amount*$porcentaje;
					$amount = $amount-$comision;
					$comisionTotal+=$comision;
					
					$nvpstr="&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".         $padDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
					"&COUNTRYCODE=US&CURRENCYCODE=$currencyCode";
					$recipient = new libpaypal();
					$recipient->init($r);
					$resArray=$recipient->hash_call("doDirectPayment",$nvpstr);
					
					$ack = strtoupper($resArray["ACK"]);
					if($ack!="SUCCESS")  
					{
						$_SESSION['reshash']=$resArray;
						$val = 1;
					}
					
					else
					{
						$this->main->ChargePay($resArray,$r->vendedor);
					}
					}
				}
				
				if($val == 0)
				{
					/*Pago a barriodescuentos */
					$nvpstr="&PAYMENTACTION=$paymentType&AMT=$comisionTotal&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".         $padDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
					"&COUNTRYCODE=US&CURRENCYCODE=$currencyCode";
					/* Make the API call to PayPal, using API signature.
					The API response is stored in an associative array called $resArray */
					$resArray=$this->libpaypal->hash_call("doDirectPayment",$nvpstr);
					/* Display the API response back to the browser.
					If the response from PayPal was a success, display the response parameters'
					If the response was an error, display the errors received using APIError.php.
					*/
					$ack = strtoupper($resArray["ACK"]);
					if($ack!="SUCCESS")  
					{
						$_SESSION['reshash']=$resArray;
						$val = 1;
					}
					else
					{
						$this->main->ChargePay($resArray,'0');
						$this->main->CompraValidate();
					}
				}
				
				if($val == 1)
				{
					$msj = 'Paypal nos ha respondido: <br/><br/>';
					echo $this->error($msj.$resArray['L_LONGMESSAGE0']);
				}
				else{
					if($tarjeta->num_rows==0)
					$this->db->insert('tarjetas',array('user'=>$_SESSION['user'],'tarjeta'=>$creditCardType,'numero'=>$creditCardNumber,'expmes'=>$expDateMonth,'expano'=>$expDateYear,'verificacion'=>$cvv2Number));
					echo $this->success('El pago ha sido completado, ya hemos recibido el pago gracias... <script>$("#btn-close").click(function(){document.location.href="'.site_url().'"})</script>');
				}
				}
				else
				echo $this->error('No existen compras a cancelar');
			}
				else
				echo $this->error($this->form_validation->error_string());
				
		}
	}
?>
