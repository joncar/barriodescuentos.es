<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('inicio.php');
class Show extends Inicio {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $msj = '';
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('main');
		$this->load->database();
		if(!empty($_POST['reg']))
			$this->msj = $this->reg();
	}
	public function v($id = '0',$title = '')
        {
           if($id == 0)$id = $title;
           $r = $this->main->getProduct($id);
           if($r){
           $r->ahorra = $r->precio*($r->descuento/100);
           $r->precio = $this->main->price($r->precio-$r->ahorra);
           $r->ahorra = $this->main->price($r->ahorra);
           $r->descuento.=" %";
           //Carga imagenes 
           $this->db->where('id_producto',$r->idProduct);
           $pic = $this->db->get('images');
           $oto = $r->status==1?'0':strtotime($r->date_final)*1000;
           $this->loadView(array('view'=>'show','oto'=>$oto,'title'=>$r->nombre,'product'=>$r,'list'=>$this->main->getList(),'pic'=>$pic));
           }
           else
           $this->loadView(array('view'=>'404'));
        }
        
        public function e($id)
        {
            $title = $id;
            $id = explode("-",$id);
            $id = $id[count($id)-1];
            $empresa = $this->main->getCompany($id);
            $this->db->where('empresa.user',$empresa->row()->user);
            $list = $this->main->getList();
            
            if($this->db->get_where('visitas',array('user'=>$_SERVER['REMOTE_ADDR'],'empresa'=>$id))->num_rows==0)
            $this->db->insert('visitas',array('user'=>$_SERVER['REMOTE_ADDR'],'empresa'=>$id));
                    
            $this->loadView(array('view'=>'sempresa','title'=>$title,'company'=>$empresa,'list'=>$list));
        }
        
        public function cat($id)
        {
            if($id!='all')
            $this->main->where('productos.id_categoria',$id);
            $r = $list = $this->main->getList();
           
            if($r)
            {
                $this->db->where('id',$id);
                $c = $this->db->get('categorias');
                $label = 'Categoria '.$id;
                if($c->num_rows>0)
                $label = $c->row()->nombre;
                $this->loadView(array('view'=>'list','list'=>$r,'label'=>$label));
            }
            else
            $this->loadView(array('view'=>'404'));
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */