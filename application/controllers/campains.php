<?php
	require_once("panel.php");
	require_once("images.php");
	class Campains extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('campanas');
			if($_SESSION['type']!='admin'){
                        $crud->where('campanas.user',$_SESSION['user']);
			$crud->set_relation('id_producto','productos','nombre',array('productos.user'=>$_SESSION['user']));
			}
			else
			$crud->set_relation('id_producto','productos','nombre');
			$crud->set_subject('Campanas');
			$crud->fields('id_producto','date_init','date_final','descuento','user','status');
                        $crud->field_type('user', 'invisible');
                        $crud->field_type('status', 'invisible');
                        $crud->required_fields('id_producto','date_init','date_final','descuento');
			$crud->callback_column('status',array($this,'getStatus'));
                        $crud->callback_before_insert(array($this,'binsertion'));
                        $crud->callback_before_update(array($this,'binsertion'));
            		if($_SESSION['type']!='admin'){
			$crud->unset_delete();
			$crud->unset_edit();
			$crud->change_field_type('status','invisible');
			}
			$crud->display_as('id_producto','Producto');
                        $crud->display_as('date_init','Inicio de publicación');
                        $crud->display_as('date_final','Fecha Final de Publicación');
			if($_SESSION['type']!='admin')
                        $crud->unset_columns('user');
			else
			$crud->set_relation('user','user','nombre');
			//Column
			$crud->callback_column('descuento',array($this->main,'percent'));
			$output = $crud->render();
			$this->dir($output,'panel_campains');
                        
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	
		}
                
                function binsertion($post)
                {
                    $post = $this->adduser($post);
                    $f = str_replace('/','-',$post['date_init']);
                    if(strtotime($f)<=strtotime(date("Y-m-d H:i:s")))
                        $post['status']=0;
                    else 
                        $post['status']=1;
                    return $post;
                        
                        
                }
		
		function fieldstatus($val = '')
		{
			return $this->add_dropdown('status',array('0'=>'Vigente','1'=>'Finalizada'));
		}

		function getStatus($val, $row)
		{
			return $val==1?'Finalizada':'Vigente';
		}
	}
?>
