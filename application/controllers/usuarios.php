<?php
	require_once("panel.php");
	class Usuarios extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('user');
			$crud->set_subject('Usuarios registrados');
			
			$crud->display_as('type','Tipo de usuario');
			$crud->display_as('pass','Contraseña');
			$crud->display_as('status','Estado');
			
			$crud->unset_columns('pass');
                        $crud->unset_add();
			$crud->unset_fields('pass');
			$crud->field_type('pass','password');
			
			$crud->callback_field('type',array($this,'type'));
			$crud->callback_field('status',array($this,'status'));
			$crud->callback_column('status',array($this,'getStatus'));
			
			$crud->callback_before_insert(array($this,'addpass'));
			$crud->callback_before_update(array($this,'addpass'));
			
			$crud->set_rules('nombre','Nombre','required');
			$crud->set_rules('apellido','Apellido','required');
			$crud->set_rules('email','Email','required|valid_email');
			
			$output = $crud->render();
			$this->dir($output,'usuarios');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	
			
		}
		
		function addpass($post_array)
		{
			$post_array['pass'] = md5($post_array['pass']);
			return $post_array;
		}
		
		function getStatus($val,$row)
		{
			switch($val)
			{
                                case '-1': $status = 'Eliminado'; break;
				case '0': $status = 'Bloqueado'; break;
				case '1': $status = 'Activo'; break;
				case '2': $status = 'Por Validar'; break;
			}
			return $status;
		}
                
		
		function type()
		{
			return form_dropdown('type',array('user'=>'Usuario','admin'=>'Administrador'));
		}
		function status()
		{
			return form_dropdown('status',array('1'=>'Activo','0'=>'Bloqueado'));
		}
		
		
	}
?>
