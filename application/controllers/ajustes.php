<?php
	require_once("panel.php");
	class Ajustes extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('ajustes');
			$crud->set_subject('Ajustes del sistema');
			$crud->unset_add();
                        $crud->unset_delete();
                        $crud->unset_fields('paypal_api_username','paypal_api_password','paypal_signature');
                        $crud->unset_columns('paypal_api_username','paypal_api_password','paypal_signature');
			$crud->display_as('monedan','Abreviatura');
			$crud->field_type('monedan','dropdown',array('USD'=>'USD','EUR'=>'EUR'));
			$crud->field_type('moneda','dropdown',array('$'=>'$','€'=>'€'));
			$crud->required_fields('moneda','monedan');
            $crud->callback_column('porcentaje',array($this->main,'percent'));
			$output = $crud->render();
			$this->dir($output,'usuarios');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	
			
		}
		
		
	}
?>
