<?php
	require_once("panel.php");
	class Tarjetas extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('tarjetas');
			$crud->set_relation('user','user','nombre');
			$crud->set_subject('Tarjetas de credito afiliadas');
			$crud->unset_edit();
			$crud->display_as('tarjeta','Tipo de tarjeta');
			$crud->display_as('numero','Numero de tarjeta');
			$crud->display_as('expmes','Mes de expiracion');
			$crud->display_as('expano','Año de expiracion');
			
			$crud->callback_column('numero',array($this,'cutnumber'));
			if($_SESSION['type']!='admin')
			{
				$crud->unset_columns('user');
				$crud->change_field_type('user','invisible');
				$crud->where('user',$_SESSION['user']);
				$crud->callback_before_insert(array($this,'binsertion'));
			}
			$crud->field_type('tarjeta','dropdown',array('Visa'=>'Visa','MasterCard'=>'MasterCard','Discover'=>'Discover','Amex'=>'American Express'));
			$crud->required_fields('tarjeta','numero','expmes','expano','verificacion');
			$crud->set_rules('tarjeta','Tipo de tarjeta','required');
			$crud->set_rules('numero','Numero de tarjeta','required|integer');
			$crud->set_rules('verificacion','Numero de Verificacion','required|integer');
			$crud->set_rules('expmes','Mes de expiracion','required');
			$crud->set_rules('expano','Año de expiracion','required');
			$output = $crud->render();
			$this->dir($output,'usuarios');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	
			
		}
		
		function binsertion($post)
		{
			if($this->db->get_where('tarjetas',array('numero'=>$post['numero']))->num_rows>0)
				return false;
			return $this->adduser($post);
		}
		
		function cutnumber($val,$row)
		{
			return cut_number($val);
		}
	}
?>
