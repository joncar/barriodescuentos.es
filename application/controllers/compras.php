<?php
	require_once("panel.php");
	class Compras extends Panel{
		var $articles = array();
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();
			$crud->set_theme('flexigrid');
			$crud->set_table('ventas');
			$crud->unset_add();
			$crud->unset_edit();
			$crud->columns('Producto','Fecha','precio');
			$crud->where('ventas.user',$_SESSION['user']);
			$crud->where('ventas.status',0);

			$crud->callback_column('Producto',array($this,'lista_getProducto'));
			$crud->callback_column('precio',array($this,'lista_getPrecio'));
			$output = $crud->render();
			$this->dir($output,'panel_compras');
			}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
			}
		}
		
		function lista_getProducto($val,$row)
		{
			$this->articles[count($this->articles)] = $row;
			$campana = $this->db->get_where('campanas',array('id'=>$row->id_campana))->row()->id_producto;
			return $this->db->get_where('productos',array('id'=>$campana))->row()->nombre;
		}
		
		function lista_getPrecio($val,$row)
		{
			$campana = $this->db->get_where('campanas',array('id'=>$row->id_campana))->row();
			$precio = $this->db->get_where('productos',array('id'=>$campana->id_producto))->row()->precio;
			return $precio-($precio * ($campana->descuento/100));
		}
	}
?>
