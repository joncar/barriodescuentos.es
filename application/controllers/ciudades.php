<?php
	require_once("panel.php");
	class Ciudades extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('ciudades');
			$crud->set_subject('Localidades registradas');
			$crud->columns('nombre','visible','emp');
			$crud->display_as("emp","Empresas registradas");
			$crud->callback_column("emp",array($this,'emp'));
			$crud->callback_field("visible",array($this,'visible'));
			$crud->callback_before_insert(array('binsertion'));
			$crud->callback_before_update(array('binsertion'));
			$crud->required_fields('nombre');
			$crud->unset_delete();
                        
                        $output = $crud->render();
			$this->dir($output,'usuarios');
                        
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	
			
		}
		
		function binsertion($post)
		{
			if(empty($_POST['visible']))
			$post['visible'] = 0;
			return $post;
		}
		
		function emp($val,$row)
		{
			$x = $this->db->get_where('empresa',array('ciudad'=>$row->id))->num_rows;
			return $x>0?$x:0;
		}
		
		function visible($val = '')
                {
					$checked = '';
					$checked2 = '';
					switch($val)
					{
						case '0': $checked2 = 'checked'; break;
						case '1': $checked = 'checked'; break;
						default : $checked2 = 'checked'; break;
					}
                    return '<input type="radio" name="visible" id="field-visible" value="1" '.$checked.'> Visible <input type="radio" name="visible" id="field-visible" value="0" '.$checked2.'> No visible';
                }
	}
?>
