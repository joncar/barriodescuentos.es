<?php
	require_once('panel.php');
	class Admin extends Panel{

		function __construct()
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('grocery_CRUD'); 
			$this->load->library('form_validation'); 
			$this->load->model('user');

			$this->js = array( 
				base_url('assets/grocery_crud/js/jquery-1.8.2.min.js'),
				base_url('assets/grocery_crud/themes/flexigrid/js/cookies.js'),
				
				base_url('assets/grocery_crud/themes/flexigrid/js/jquery.form.js'),
				base_url('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js'),
				base_url('assets/grocery_crud/themes/flexigrid/js/jquery.printElement.min.js'),
				base_url('assets/grocery_crud/js/jquery_plugins/jquery.fancybox.pack.js'),
				base_url('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js')
			);
			$this->css = array(base_url('assets/grocery_crud/themes/flexigrid/css/flexigrid.css'),
				     base_url('assets/grocery_crud/js/jquery_plugins/fancybox/jquery.fancybox.css')
			);

			ini_set('date.timezone', 'America/Caracas');
		}
		
		function index()
		{
			$this->dir((object)array('output' => '' ,
			'js_files' => $this->js , 
			'css_files' => $this->css,
			'view'=>'panel_admin'));
		}
	}
?>
