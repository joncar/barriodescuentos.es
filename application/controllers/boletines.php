<?php
	require_once("panel.php");
	class Boletines extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('boletines');
			$crud->set_subject('boletin');
			$crud->set_relation('user','user','nombre');
			$crud->required_fields('titulo','texto','frecuencia','destinatarios','timer');
			$crud->columns('titulo','destinatarios','frecuencia','texto','fecha','user','send','timer');
			$crud->callback_column('send',array($this,'send'));
			$crud->change_field_type('fecha','invisible');
			$crud->change_field_type('user','invisible');
                        $crud->callback_field('frecuencia',array($this,'frecuencia'));
                        $crud->callback_field('destinatarios',array($this,'destinatarios'));
			$crud->callback_before_insert(array($this,'adduser'));
			$crud->display_as('send','Enviar boletin');
			$crud->display_as('user','Creador');
                        $crud->display_as('timer','Comienza en:');
			$output = $crud->render();
			$this->dir($output,'boletin');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	
			
		}
                
                function frecuencia($val = 1)
                {
                    return form_dropdown('frecuencia',array(''=>'Seleccione la frecuencia de envio','30'=>'Una vez al mes','7'=>'Una vez a la semana','3'=>'Cada 3 dias','1'=>'Diario'),$val,'id="field-frecuencia"');
                }
                
                function destinatarios($val= '')
                {
                    return '
                        <input type="checkbox" name="boletin" checked id="user1" value="normal"> Usuarios normales<br/>
                        <input type="checkbox" name="boletin" id="user2" value="porvalidar"> Usuarios por validar<br/>
                        <input type="checkbox" name="boletin" id="user3" value="empresa"> Propietarios de empresas<br/>
                        <input type="checkbox" name="boletin" id="user4" value="admin"> Administradores<br/>
                        <input type="checkbox" name="boletin" id="user5" value="inactivo"> Usuarios inactivos<br/>
                        <input type="hidden"   name="destinatarios" id="destinatarios" value="normal">';
                }
		
		function send($val,$row)
                {
                    return '<a href="javascript:ajax(\'\',undefined,undefined,undefined,\''.base_url('inicio/loadViewAjax/sendBoletin/'.$row->id).'\')" class="btn btn-primary btn-small" title="Enviar"><i class="icon-white icon-envelope"></i></a>';
                }
		
		
	}
?>
