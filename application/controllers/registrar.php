<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('inicio.php');
class Registrar extends Inicio {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $msj = '';
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->database();
		if(!empty($_POST['reg']))
			$this->msj = $this->reg();
                if(!empty($_SESSION['user']))
                header("Location:".base_url('panel'));
	}

	private function reg()
	{
		
		$this->form_validation->set_rules('name','Nombre','required');
		$this->form_validation->set_rules('apellido','Apellido','required');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('pass','Password','required|min_length[8]');
		$this->form_validation->set_rules('pass2','Password','required');

		if($this->form_validation->run() && $this->input->post('pass') == $this->input->post('pass2'))
		{
			$this->db->where('email',$this->input->post('email'));
                        $u = $this->db->get('user');
			if($u->num_rows == 0){
			$data = array('nombre'=>$this->input->post('name',TRUE),
						  'apellido'=>$this->input->post('apellido',TRUE),
						  'email'=>$this->input->post('email',TRUE),
						  'pass'=>md5($this->input->post('pass',TRUE)),
						  'type'=>'user',
						  'status'=>'2');
			$this->db->insert('user',$data);
			$codigo = md5(rand(0,255));
			$this->db->insert('activacion',array('user'=>$this->input->post('email',TRUE),'codigo'=>$codigo));
			mail_activate($this->input->post('email',TRUE),$this->input->post('name',TRUE),$codigo);
			return $this->success('Registro correcto... Revise su correo electronico para activar su cuenta.');
			}
                        else if($u->row()->status=='-1')
                        {
                            $data = array('nombre'=>$this->input->post('name',TRUE),
                                          'apellido'=>$this->input->post('apellido',TRUE),
					  'email'=>$this->input->post('email',TRUE),
					  'pass'=>md5($this->input->post('pass',TRUE)),
                                          'status'=>'0');
                            
                            $this->db->where('email',$this->input->post('email'));
                            $this->db->update('user',$data);
                            
                            $codigo = md5(rand(0,255));
                            $this->db->where('user',$this->input->post('email',TRUE));
                            $this->db->update('activacion',array('codigo'=>$codigo));
                            mail_activate($this->input->post('email',TRUE),$this->input->post('name',TRUE),$codigo);
                            return $this->success('Registro correcto... Revise su correo electronico para activar su cuenta.');
                        }
			else
			return $this->error('El email que intenta utilizar ya se encuentra registrado, si olvido su contrasena pulse <a href="#">aqui</a>');		
		}
		else return $this->error($this->form_validation->error_string());
	}
	
	public function activate($email,$codigo)
	{
		$email = urldecode($email);
		if(empty($email) || empty($codigo))
			$this->loadView(array('view'=>'404'));
		else
		{
			$this->db->where('user',$email);
			$this->db->where('codigo',$codigo);
			
			$r = $this->db->get('activacion');
			if($r->num_rows>0)
			{
				$this->db->where('user',$email);
				$this->db->delete('activacion');
				
				$this->db->where('email',$email);
				$this->db->update('user',array('status'=>'1'));
                                $this->user->short_login($email);
                                header("Location: ".base_url('empresa/index/add'));
				//$this->loadView(array('view'=>'activate','msj'=>$this->success('Cuenta activada con exito, a partir de este momento puedes comenzar a publicar cupones')));
			}
			else
			$this->loadView(array('view'=>'activate','msj'=>$this->error('Ha ocurrido un error con la activación del sistema, comunicate con un administrador en soporte@barriodescuentos.com')));
		}
		
	}
	
	public function forget()
	{
		if(empty($_POST['email']))
		{
			$this->loadView(array('view'=>'passrecover','msj'=>''));
		}
		else
		{
			$this->form_validation->set_rules('email','Email','required|valid_email');
			if($this->form_validation->run())
			{
			$this->db->where('email',$this->input->post('email',TRUE));
			if($this->db->get('user')->num_rows>0)//success
			{
				$codigo = md5(rand(0,255));
				$this->db->insert('activacion',array('user'=>$this->input->post('email',TRUE),'codigo'=>$codigo));
				mail_recover($this->input->post('email',TRUE),$codigo);
				$this->loadView(array('view'=>'passrecover','msj'=>$this->success('Se ha enviado los pasos de restablecimiento a su correo electronico')));
			}
			else
			$this->loadView(array('view'=>'passrecover','msj'=>$this->error('No se ha encontrado el email que intenta recuperar, verifiquelo e intenté nuevamente')));
			}
			else
			$this->loadView(array('view'=>'passrecover','msj'=>$this->error($this->form_validation->error_string())));
		}
	}
	
	public function recover($email = '',$codigo = '')
	{
		$email = urldecode($email);
		if(!empty($_POST['email']) && !empty($_POST['pass']) && !empty($_POST['pass2']) && !empty($_POST['codigo']))
		{
			$this->form_validation->set_rules('email','Email','required|valid_email');
			$this->form_validation->set_rules('pass','Contraseña','required|min_length[8]');
			if($this->input->post('pass',TRUE) == $this->input->post('pass2',TRUE))
			{
				if($this->form_validation->run())
				{
				$this->db->where('user',$this->input->post('email',TRUE));
				$this->db->where('codigo',$this->input->post('codigo',TRUE));
				if($this->db->get('activacion')->num_rows>0)
				{
						$this->db->where('email',$this->input->post('email',TRUE));
						$r = $this->db->update('user',array('pass'=>md5($this->input->post('pass',TRUE))));
						$this->db->where('user',$this->input->post('email',TRUE));
						$this->db->delete('activacion');
						$this->loadView(array('view'=>'recover','msj'=>$this->success('Su contraseña ha sido reestablecida correctamente'),'email'=>$email,'codigo'=>$codigo));
				}
				else
				$this->loadView(array('view'=>'recover','msj'=>$this->error('Ha habido un problema con la activación'),'email'=>$email,'codigo'=>$codigo));
				}
				else
				$this->loadView(array('view'=>'recover','msj'=>$this->error($this->form_validation->error_string()),'email'=>$email,'codigo'=>$codigo));
			}
			else
			$this->loadView(array('view'=>'recover','msj'=>$this->error('Las contraseñas deben ser iguales'),'email'=>$email,'codigo'=>$codigo));
		}
		else
		{
		if(!empty($email) && !empty($codigo))
		{
			$this->db->where('user',$email);
			$this->db->where('codigo',$codigo);
			
			$r = $this->db->get('activacion');
			if($r->num_rows>0)
			{
				$this->loadView(array('view'=>'recover','msj'=>'','email'=>$email,'codigo'=>$codigo));
			}
			else
			$this->loadView(array('view'=>'404'));
		}
		else
		$this->loadView(array('view'=>'404'));
		}
	}
	
	public function index()
	{
		$this->load->view('template',array('view'=>'reg'));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */