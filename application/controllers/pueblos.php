<?php
	require_once("panel.php");
	class Pueblos extends Panel{
		function __construct()
		{
			parent::__construct();   
		}
		
		function index()
		{
	
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('pueblos');
			$crud->set_subject('Pueblos registrados');
			$crud->set_relation('id_ciudad','ciudades','nombre');
			$crud->display_as('id_ciudad','Ciudad');
			$crud->display_as('nombre','Pueblo');
			$crud->required_fields('id_ciudad','nombre');
			$crud->unset_delete();
                        
            $output = $crud->render();
			$this->dir($output,'usuarios');
                        
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	
			
		}
	}
?>
