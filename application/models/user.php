<?php

	class User extends CI_Model{
		var $log = false;

		function __construct(){
			parent::__construct();
			if(!empty($_SESSION['user']))
				$this->log = $_SESSION['user'];
		}

		function login($user,$pass)
		{
			$this->db->where('email',$user);
			$this->db->where('pass',md5($pass));
			
			$r = $this->db->get('user');
			if($r->num_rows>0 && $r->row()->status==1)
			{
				$r = $r->row();
				$_SESSION['user']=$r->id;
                                $_SESSION['type']=$r->type;
                                $_SESSION['cuenta']=$r->type;
                                $_SESSION['nombre']=$r->nombre;
				$_SESSION['apellido']=$r->apellido;
				return true;
			}
			else
				return false;
		}
                
                function short_login($user)
                {
                   $this->db->where('email',$user);
                   $r = $this->db->get('user');
                   if($r->num_rows>0 && $r->row()->status==1)
                    {
				$r = $r->row();
				$_SESSION['user']=$r->id;
                                $_SESSION['type']=$r->type;
                                $_SESSION['cuenta']=$r->type;
                                $_SESSION['nombre']=$r->nombre;
				$_SESSION['apellido']=$r->apellido;
				return true;
                    }
                    else
				return false;
                }

		function unlog()
		{
			session_unset();
		}
		
		function edit($data)
		{
			$this->db->where('id',$_SESSION['user']);
			$this->db->update('user',$data);
		}

	}

?>