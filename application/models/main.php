<?php

class Main extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    //Añade una clausula a las consultas
    public function where($field,$val)
    {
        $this->db->where($field,$val);
    }
	//COnsulta el contenido de una pagina
	public function getPage($url)
	{
		if($r = $this->db->get_where('paginas',array('nombre'=>$url)))
			return $r->row();
		else
			return false;
	}
	function plugins_convert_string($texto)
	{
		//Si se imprimen las noticias
		if(strstr($texto,'[noticias]')){
		$texto = noticias($this->getNotice(),$texto);}
		if(strstr($texto,'[offerday]')){
		$texto = offerday($this->getOfferDay(),$texto);}
		if(strstr($texto,'[list]')){
		$texto = showListProduct($this->getList(),$texto);}
        return $texto;
	}
        
        function getCompany($id)
        {
            $this->db->join('categorias','categorias.id = empresa.categoria','inner');
            $this->db->join('user','user.id = empresa.user','inner');
            $this->db->join('ciudades','ciudades.id = empresa.ciudad','inner');
            $this->db->select('empresa.*,empresa.nombre as company,empresa.direccion as empdirec, empresa.id as idCompany, categorias.nombre as cat, ciudades.nombre as ciudadn,user.*');
            return $this->db->get_where('empresa',array('empresa.id'=>$id));
        }
		
	//Trae la lista de noticias
	public function getNotice()
	{
                $x = FALSE;
                if(!empty($_SESSION['user'])){
                    if($_SESSION['type']=='admin')
                    $this->db->like('permiso','administradores');
                    else{
                        $propietario = $this->db->get_where('empresa',array('user'=>$_SESSION['user']))->num_rows;
                        if($propietario>0)
                            $this->db->like('permiso','propietarios');
                        else
                            $this->db->like('permiso','registrados');
                    }
                }
                else
                $this->db->like('permiso','visitantes');
                $this->db->order_by('fecha');
                $this->db->limit('10');
		return $this->db->get('noticias');
	}
        
        function getNoticeReg()
        {
            $this->db->like('permiso','registrados');
            $this->db->order_by('fecha');
            return $this->db->get('noticias');
        }
    //Transforma de numero a moneda
    public function price($val)
    {
	return number_format($val,2,',','.').' '.$this->db->get('ajustes')->row()->moneda;
    }
	public function price_avr()
    {
		return $this->db->get('ajustes')->row()->monedan;
    }
    //Añade el simbolo de porcentaje
    public function percent($val)
    {
	return $val.' %';
    }
    //Trae la oferta del dia
    function getOfferDay()
    {
        $this->db->select('campanas.*,campanas.id as idCampana, productos.*, images.*');
        $this->db->join('productos','productos.id = campanas.id_producto','inner');
        $this->db->join('images','productos.id = images.id_producto','left');
		$this->db->where('campanas.date_final >=',date("Y-m-d H:i:s"));
		$this->db->order_by('date_final');
                $this->db->where('campanas.status','0');
        $x = $this->db->get('campanas');
		if($x->num_rows>0)
		{
			$x = $x->row();
			$x->nombre = 'Paga '.$this->price($x->precio-($x->precio*($x->descuento/100)))." por ".$x->nombre;
			$x->date_final = date("d/m/Y H:i:s",strtotime($x->date_final));
			$x->hora = date("H:i:s",strtotime($x->date_final));
			return $x;
		}
		else
			return false;
    }
    //Trae la lista del resultado de busqueda
    function getList()
    {

        $this->db->select('campanas.*, campanas.id as idCampana,productos.id as idProducto, productos.id_categoria as prodCat, productos.*, images.*');
        $this->db->join('productos','productos.id = campanas.id_producto','inner');
        $this->db->join('images','productos.id = images.id_producto','left');
		$this->db->join('empresa','empresa.id = productos.empresa','inner');
		$this->db->join('ciudades','ciudades.id = empresa.ciudad','inner');
		if(!empty($_SESSION['ciudad']))
		{
			$this->db->where('ciudades.nombre',$_SESSION['ciudad']);
		}
		//$this->db->where('campanas.date_final >=',date("Y-m-d H:i:s"));
        $this->db->where('campanas.status','0');
        $this->db->group_by('idCampana');
        $r = $this->db->get('campanas');
		
		for($i=0;$i<$r->num_rows;$i++){
		$r->row($i)->url = empty($r->row($i)->url)?'img/vacio.png':'assets/uploads/'.$r->row($i)->url;
		$r->row($i)->nombre = 'Paga '.$this->price($r->row($i)->precio-($r->row($i)->precio*($r->row($i)->descuento/100)))." por ".$r->row($i)->nombre;
        }
        
        return $r;
    }
    //Trae el detalle del producto seleccionado
    function getProduct($id = 1)
    {
        $this->db->select('campanas.*,productos.id as idProduct,empresa.facebook,empresa.twitter,empresa.nombre as compania,ciudades.nombre as ciudad, empresa.telefono, user.email, categorias.nombre as categoria, campanas.id as idCampana, productos.*, images.*');
        $this->db->join('productos','productos.id = campanas.id_producto','inner');
        $this->db->join('images','productos.id = images.id_producto','left');
        $this->db->join('empresa','empresa.id = productos.empresa','inner');
        $this->db->join('categorias','categorias.id = productos.id_categoria','inner');
        $this->db->join('user','user.id = productos.user','inner');
        $this->db->join('ciudades','ciudades.id = empresa.ciudad','inner');
        $this->db->where('campanas.id',$id);
        
        $r = $this->db->get('campanas')->row();
        if($r){
        $this->db->where('ventas.id_campana',$r->idCampana);
        $r->ventas = $this->db->get('ventas')->num_rows;
        }
		
        return $r;
    }
    //trae la consulta de las ventas realizadas
    function getVentas()
    {
        $this->db->select('productos.nombre,  ventas.fecha, user.nombre as comprador, productos.precio, campanas.descuento');
        $this->db->join('ventas','ventas.id_campana = campanas.id','inner');
        $this->db->join('user','user.id = ventas.user','inner');
        $this->db->join('productos','productos.id = campanas.id_producto','inner');
        $this->db->where('campanas.user',$_SESSION['user']);
		$this->db->where('ventas.status','1'); //Descomentar cuando se elabore el sistema de pagos
        $r = $this->db->get('campanas');
        return $r;
    }
    //Cantidad de cleintes registrados
    function getClientesReg()
    {
        return $this->db->get('user')->num_rows;
    }
     //Cantidad de productos registrados
    function getProductsReg()
    {
        return $this->db->get('productos')->num_rows;
    }
    //query para seleccionar las ventas
    function getSellQuery()
    {
        $this->db->select("productos.precio, campanas.descuento");
        $this->db->from("ventas");
        $this->db->join("campanas","campanas.id = ventas.id_campana",'inner');
        $this->db->join("productos","productos.id = campanas.id_producto","inner");
        $this->db->where("campanas.user",$_SESSION['user']);
    }
    //Total ventas
    function getTotalSell()
    {
        $sum = 0;
        $this->getSellQuery();
        foreach($this->db->get()->result() as $x)
        {
            $sum += $x->precio-($x->precio*($x->descuento/100)); 
        }   
        return $this->price($sum);   
    }
    //Total ventas del dia
    function getTotalSellDay()
    {
        $this->getSellQuery();
        $this->db->where("Date(ventas.Fecha)",date("Y-m-d"));
        $sum = 0;
        foreach($this->db->get()->result() as $x)
        {
            $sum += $x->precio-($x->precio*($x->descuento/100)); 
        }
         return $this->price($sum);   
    }
	
	/*********** Modulo de pagos ***********/
	function getCompra()
	{
		$this->db->select('productos.precio,campanas.user as vendedor, campanas.descuento,user.email,empresa.*');
		$this->db->join('campanas','campanas.id = ventas.id_campana','inner');
		$this->db->join('productos','campanas.id_producto = productos.id','inner');
		$this->db->join('user','user.id = ventas.user','inner');
		$this->db->join('empresa','productos.empresa = empresa.id','inner');
		$this->db->where('ventas.user',$_SESSION['user']);
		$this->db->where('ventas.status',0);
		
		$r = $this->db->get('ventas');
		for($i=0;$i<$r->num_rows;$i++)
			$r->row($i)->vendedor_email = $this->db->get_where('user',array('id'=>$r->row($i)->vendedor))->row()->email;
		
		return $r;
	}
	
	function getTotalCompra()
	{
		$r = $this->getCompra();
		if($r->num_rows>0)
		{
		$sub = 0;
		foreach($r->result() as $x)
		$sub+= $x->precio-($x->precio*($x->descuento/100)); 
		$r = $r->row();
		$r->subtotal = $this->price($sub);
		$r->iva = '12%';
		$r->total = $sub+($sub*($r->iva/100));
		$r->totalpago = $this->price($r->total);
		return $r;
		}
	}
	
	function CompraValidate()
	{
		$this->db->where('ventas.user',$_SESSION['user']);
		$this->db->where('ventas.status',0);
		$this->db->update('ventas',array('ventas.status'=>1));
	}
	
	function ChargePay($response,$vendedor)
	{
		$data = array();
		$data['transaccion'] = $response['TRANSACTIONID'];
		$data['fecha'] = date("Y-m-d H:i:s");
		$data['monto'] = $response['AMT'];
		$data['status'] = $response['ACK'];
		$data['comprador'] = $_SESSION['user'];
		$data['vendedor'] = $vendedor; 
		$this->db->insert('pagos',$data);
	}
	
	function getHucha()
	{	
		$this->db->select('productos.precio, campanas.descuento');
		$this->db->join('campanas','campanas.id = ventas.id_campana','inner');
		$this->db->join('productos','productos.id = campanas.id_producto','inner');
		$this->db->where('ventas.user',$_SESSION['user']);
		$r = $this->db->get('ventas');
		$hucha = 0;
		foreach($r->result() as $x)
		$hucha += $x->precio*($x->descuento/100);
		return $this->price($hucha);
	}
	
	function uploadCupons()
	{
		$data = array('status'=>'1');
		$campanas = $this->db->get_where('campanas',array('date_final'=>date("Y-m-d")));
                if($campanas->num_rows>0)
                {
                    foreach($campanas->result() as $c)
                    {
                        if(strtotime($c->date_final)<strtotime(date("Y-m-d H:i:s"))){
                        $this->db->where('id',$c->id);
                        $this->db->update('campanas',$data);    
                        }
                    }
                }
                
                $data = array('status'=>'0');
                $campanas = $this->db->get_where('campanas',array('date_init'=>date("Y-m-d")));
                if($campanas->num_rows>0)
                {
                    foreach($campanas->result() as $c)
                    {
                        if(strtotime($c->date_init)>strtotime(date("Y-m-d H:i:s"))){
                        $this->db->where('id',$c->id);
                        $this->db->update('campanas',$data);    
                        }
                    }
                }
	}
	
	 function sendBol($id,$rem,$dest)
        {
            
            $l = $this->db->get_where('boletines',array('id'=>$id));
            if($l->num_rows>0)
            {
				$l = $l->row();
				$dest = explode(";",$dest);
				$enviados = array();
				foreach($dest as $x)
				{
					switch($x)
					{
						case 'normal':     $this->db->where('user.type','user'); $w = 1; break; 
						case 'empresa':    $this->db->join('empresa','empresa.user=user.id','inner'); break;
						case 'admin':      $this->db->where('user.type','admin'); $w = 1; break;
						case 'inactivo':   $this->db->where('user.status','0'); $w = 1; break;
						case 'porvalidar': $this->db->where('user.status','2'); $w = 1; break;
					}
					$r = $this->db->get('user');
					if($r->num_rows>0)
					{
						$content = $this->plugins_convert_string($l->texto);
                                                $content = $this->load->view('mintemplate',array('content'=>$content),TRUE);
						foreach($r->result() as $x){
							if(!in_array($x->email,$enviados)){
							correo($x->email,$l->titulo,$content,'BarrioDescuentos '.$rem);
							$enviados = array_merge(array($x->email),$enviados);
							}
						}
					}
				}
				return true;
            }
            else
                return false;
        }
}
?>
