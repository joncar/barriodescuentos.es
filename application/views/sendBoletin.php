<div id="alert-boletin" class="alert alert-info">
    <h1>¿A quienes le enviamos el boletin?</h1>
    <form onsubmit="return send()"><br/>
        
        Remitente: <input type="text" name="email" id="email" value="info@barriodescuentos.es"><br/>
        Destinatarios:<br/>
        <input type="checkbox" name="dest" checked id="user1" value="normal"> Usuarios normales<br/>
	<input type="checkbox" name="dest" id="user2" value="porvalidar"> Usuarios por validar<br/>
        <input type="checkbox" name="dest" id="user3" value="empresa"> Propietarios de empresas<br/>
	<input type="checkbox" name="dest" id="user4" value="admin"> Administradores<br/>
	<input type="checkbox" name="dest" id="user5" value="inactivo"> Usuarios inactivos<br/><br/>
        <input type="submit" name="boletin" id="boletin" value="Enviar" class="btn btn-success">
    </form>
</div>
<script>
    function send()
    {
       
        
        if($("#email").val()!='')
        {
            var dest = '';
			for(var i=1;i<6;i++)
			{
				if($("#user"+i).prop('checked'))
					dest = dest+$("#user"+i).val()+";";
			}
            
            ajax('id=<?= $id ?>&email='+$("#email").val()+'&dest='+dest,true,"#alert-boletin",undefined,'<?= base_url('inicio/sendBol') ?>')
        }
        return false;
    }
</script>
