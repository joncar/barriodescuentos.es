<!--            <li><a href="<?= base_url('compras'); ?>"><i class="icon icon-shopping-cart"></i> Capturas</a></li>-->
            <li><a href="<?= base_url('inicio/edit_user_form') ?>"><i class="icon icon-edit"></i> Datos de usuario</a></li>
                   
            <li><a href="<?= site_url('notice') ?>"><i class="icon icon-globe"></i> Noticias</a></li>
            <? if($this->db->get_where('empresa',array('user'=>$_SESSION['user']))->num_rows>0): ?>
            <li class="divider" role="presentation"></li>
            <li><a href="<?= base_url('empresa') ?>"><i class="icon icon-briefcase"></i> Empresas Registradas</a></li>
            <li><a href="<?= base_url('inicio/index/informacion') ?>"><i class="icon icon-globe"></i> información para el comercio</a></li>
            <li><a href="<?= base_url('productos') ?>"><i class="icon icon-briefcase"></i> Productos</a></li>
            <li><a href="<?= base_url('campains') ?>"><i class="icon icon-briefcase"></i> Campañas</a></li>
                    
                    <? else : ?>
                    <li><a href="<?= base_url('empresa') ?>"><i class="icon icon-briefcase"></i> ¿Tienes un negocio?</a></li>
                    <? endif ?>
                    <? if($_SESSION['type']=='admin'): ?>
                    <li class="divider" role="presentation"></li>
                     <li class="dropdown-submenu">
                        <a href="<?= base_url('admin') ?>"><i class="icon icon-wrench"></i> Administrar</a>
                        <ul class="dropdown-menu">
                               <li><a href="<?= base_url('ajustes') ?>">Ajustes Generales</a></li>
                               <li><a href="<?= base_url('boletines') ?>">Boletines</a></li>
                               <li class="divider"></li>
                               <li><a href="<?= base_url('categorias') ?>">Categorias</a></li>
                               <li><a href="<?= base_url('ciudades') ?>">Localidades</a></li>
                               <!--<li><a href="<?= base_url('pueblos') ?>">Pueblos</a></li>-->
                               <li><a href="<?= base_url('paginas') ?>">Paginas</a></li>
                               <li><a href="<?= base_url('noticias') ?>">Noticias</a></li>
                               <li><a href="<?= base_url('usuarios') ?>">Usuarios</a></li>
                        </ul>
                    </li>
                    <? endif ?>
                    <li class="divider" role="presentation"></li>
                    <li><a href="<?= base_url('inicio/unlog'); ?>"><i class="icon icon-off"></i> Salir</a></li>
