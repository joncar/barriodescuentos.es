<? if($company->num_rows>0): $company = $company->row();?>
<? 
        $c = str_replace('(','',$company->ubicacion);
        $c = str_replace(')','',$c);
        $p = explode(",",$c);
        if(count($p)>1)
        list($x,$y) = explode(",",$c);
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
<section>
    <article class="row-fluid well" style="padding:0px;">
    <div class="span3" style="margin-left:20px">
    <img class="img-polaroid" src="<?= base_url('assets/uploads/maps/'.$company->logo) ?>" style="width:300px">
    </div>
    <div class="span4" style="text-align:left">
        <h1><?= $company->company ?></h1>
        <div style="border-bottom:1px solid gray;"></div>
        <?= $company->descripcion ?>
        <p>
            <div class="fb-like" data-href="<?= site_url('show/e/'.str_replace('+','-',urlencode($company->company))."-".$company->idCompany) ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
            <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?= site_url('show/e/'.str_replace('+','-',urlencode($company->company))."-".$company->idCompany) ?>" data-text="<?= $company->company ?>" data-lang="es">Twittear</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            <div class="g-plusone" data-size="medium"></div>
            <script type="text/javascript">window.___gcfg = {lang: 'es-419'};(function() {var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;po.src = 'https://apis.google.com/js/plusone.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);})();</script>
    </div>
    <div class="span3">
     <h4>Visitas</h4>
     <h5><?= $this->db->get_where('visitas')->num_rows; ?></h5>
     <h4>Seguidores</h4>
     <h5><?= $this->db->get_where('follows',array('empresa'=>$company->idCompany))->num_rows ?></h5>
     <? if(!empty($_SESSION['user']) && $this->db->get_where('follows',array('user'=>$_SESSION['user'],'empresa'=>$company->idCompany))->num_rows==0): ?><a href="javascript:ajax('e=<?= $company->idCompany ?>',undefined,undefined,undefined,'<?= base_url('inicio/follow')?>')" class="btn btn-success">+<i class="icon-white icon-user"></i> Seguir</a><?endif?>
    </div>
    </article>
</section>
<div class="row-fluid" style="padding:22px 0;">
    <div class="span3 well">
        <p><?= $company->direccion ?></p>
        <p><b>Horario de atención: </b><br><?= $company->horario ?></p>
        <p><b>Categoria: </b><?= $company->cat ?></p>
        <p><b>Ciudad: </b><?= $company->ciudadn ?></p>
        <p><b>Telefono: </b><?= $company->telefono ?></p>
        <p><b>Correo: </b><?= $company->email ?></p>
        <? if(!empty($company->facebook) || !empty($company->twitter)): ?>
        <p><b>Nuestras redes: </b>
            <? if(!empty($company->facebook)): ?><i class="icon icon-facebook"></i> <?= $company->facebook ?> <?endif?>
            <? if(!empty($company->twitter)): ?><i class="icon icon-twitter"></i> <?= $company->twitter ?><?endif?>
        </p>
        <? endif ?>
        <p><b>Ubicación: </b></p>
        <?= $company->empdirec ?>
        <div id="map" style="height:230px">Mapa</div>
        <div align="center">
        <?
            $params['data'] = $company->company." ".site_url('show/e/'.str_replace('+','-',urlencode($company->company))."-".$company->idCompany);
            $params['level'] = 'M';
            $params['size'] = 3;
            $params['savename'] = FCPATH.'tes.png';
            $this->ciqrcode->generate($params);
            echo '<img src="'.base_url().'tes.png" />';
        ?>
            <br>Escanee este codigo para ver nuestras ofertas facilmente
        </div>
    </div>
    <div class="span1"></div>
    <div class="span8">
        <div class="span12">
            <h1>Ofertas publicadas</h1>
        </div>
	<section class="span11" style="position:relative">
            <?= showListProduct($list); ?>
	</section>
    </div>
</div>
<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDBv7rAHZNUTj1V0rLfdTB7gEtcO7rc9QE&sensor=true">
    </script>
    <? if(count($p)>1): ?>
    <script type="text/javascript">
        $(document).ready(function(){
        var mapOptions = {
        zoom: 4,
        center: new google.maps.LatLng(<?= $x ?>, <?= $y ?>),
        mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('map'),mapOptions);
        var marker = new google.maps.Marker({
        position: map.getCenter(),
        map: map,
        title: '<?= $company->company ?>'
        });
        });
        function resizemap()
        {
            window.open('<?= base_url('inicio/map/'.trim($x)."/".trim($y)) ?>','<?= $company->company ?> - Ubicacion','width=800, height=600')
        }
    </script>
    <?endif ?>
<? else: ?>
<? header("Location:".site_url()) ?>
<? endif ?>
