<div class="oferta">
        <div class="item">
        <h1>Administración</h1>
        <table>
            <tbody>
                <tr><td>Clientes Registrados: </td><td><span class="label label-info"><?= $this->main->getClientesReg() ?></span></td></tr>
                <tr><td>Productos Registrados: </td><td><span class="label label-info"><?= $this->main->getProductsReg() ?></span></td></tr>
        </tbody></table>
        </div><div class="item">
        <h1>Finanzas</h1>
        <table>
                <tr><td>Total ventas: </td><td><span class="label label-info"><?= $this->main->getTotalSell() ?></span></td></tr>
                <tr><td>Ventas del dia: </td><td><span class="label label-info"><?= $this->main->getTotalSellDay() ?></span></td></tr>
                <tr><td>Total comision: </td><td><span class="label label-info">100 $</span></td></tr>
        </table>
        </div>
        <a href="<?= base_url('ajustes') ?>" class="btn btn-success">Ajustes del sistema</a>
    </div>