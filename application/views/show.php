<script src="<?= base_url('assets/countdown.js') ?>" type="text/javascript"></script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js">{lang: 'es'}</script>
<section>
<article class="row-fluid well" style="padding:0px;">
                <div id="myCarousel" class="span6 carousel slide" >
                <ol class="carousel-indicators">
                    
                <? $x = 'class="active"'; $i=0; foreach($pic->result() as $row):?>
                <li data-target="#myCarousel" data-slide-to="<?= $i ?>"<?= $x ?>></li>
                <? $x = ''; $i++;?>
                <? endforeach ?>
                </ol>
                <!-- Carousel items -->
                <div class="carousel-inner" style="width:100%;">
                <? $x = 'active'; $i=0; foreach($pic->result() as $row):?>
                
                <div class="<?= $x ?> item"><img src="<?= base_url('assets/uploads/'.$row->url) ?>" /></div>
                <? $x = ''; $i++;?>
                <? endforeach ?>
                </div>
                <!-- Carousel nav -->
                <? if($pic->num_rows>1): ?>
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                <? endif ?>
                </div>
            
                <div class="span5" style="padding-right: 40px;">
                <h1><?= $product->nombre ?></h1>
		<p style="margin: 0 0 30px;"><?= $product->descripcion ?></p>
		<div align="left">Finaliza en: <span class="">
                      <script type="application/javascript">
                        var myCountdown2 = new Countdown({
			time: <?= $oto ?>, 
			width:200, 
			height:38, 
			rangeHi:"day",
                        numbers	:{font 	: "Arial"}});
                    </script>
                        
                    </span><br/></div>
		
                <div class="row-fluid">
                <div class="span2">
                <h5>Precio</h5>
                <p><?= $product->precio ?></p>
                </div>
                <div class="span2">
                <h5>Descuento</h5>
                <p><?= $product->descuento ?></p>
                </div>
                <div class="span2">
                <h5>Ahorra</h5>
                <p><?= $product->ahorra ?></p>
                </div>
                <div class="span2">
                <h5>Comparte</h5>
                    <div class="row-fluid">
                        <div class="span4"><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script><a href="https://twitter.com/intent/tweet?text=<?= $product->nombre ?>&url=<?= site_url('show/v/'.$product->id) ?>&related=episod"><img src="<?= base_url('img/twitter.png') ?>"  width="32"></a></div>
                        <div class="span4"><a href="https://www.facebook.com/sharer/sharer.php?u=<?= site_url('show/v/'.$product->id) ?>" target="_blank"><img src="<?= base_url('img/facebook.png') ?>" width="32"></a></div>
                        <div class="span4"><a href="https://plus.google.com/share?url=<?= site_url('show/v/'.$product->id) ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="https://www.gstatic.com/images/icons/gplus-32.png" alt="Compartir en Google+"/></a></div>
                    </div>
                </div>
                    <div class="span4">
		<? if ($product->status == 0): ?>
		<!-- <a href="javascript:sell('<?= $product->idCampana ?>')" class="btn btn-primary btn-large"><i class="icon-white icon-shopping-cart"></i> Capturar</a>-->
		<? else: ?>
		<button class="btn btn-danger">Promoción Finalizada</button>
		<? endif ?>
                </div>
            </div></div>
        </article>
	</section>
    
    <div class="row-fluid" style="width:98%">
        <div class="span2 well">
            <h3>Tienda</h3>
            <a href="<?= site_url('show/e/'.str_replace("+","-",urlencode($product->compania."-".$product->empresa))) ?>"><?= $product->compania ?></a>
            <p><b>Ciudad: </b><?= $product->ciudad ?></p>
            <p><b>Categoria: </b><br><?= $product->categoria ?></p>
            <p><b>Contacto: </b><br><?= $product->telefono ?><br><?= $product->email ?><br><i class="icon icon-facebook"></i><?= $product->facebook ?> <i class="icon icon-twitter"></i><?= $product->twitter ?></p>
        </div>
        <div class="span10">
        <div class="span12">
            <h1>Relacionados</h1>
        </div>
	<section class="span11" style="position:relative">
            <?= showListProduct($list); ?>
	</section>
        </div>
    </div>
<script>
    function sell(id)
    {
        ajax('',undefined,undefined,undefined,'<?= base_url('inicio/sell/') ?>/'+id);
    }
</script>