<table>
    <thead style="background:#1B1B1B;">
        <tr><th style="text-align:left"><a class="brand" style="float:left" href="<?= base_url() ?>"><img src="<?= base_url('img/logo.png') ?>" /></a></th></tr>
    </thead>
    <tbody>
        <tr><td><?= $content ?></td></tr>
    </tbody>
    <tfoot  style="background:#1B1B1B; color:white">
        <tr><td style="text-align:center"><a class="brand" href="<?= base_url() ?>">BarrioDescuentos <?= date("Y") ?>. Todos los derechos reservados</a></td></tr>
    </tfoot>
</table>