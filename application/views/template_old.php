<!DOCTYPE html>
<html lang="en">
<head>
	<title><?= !empty($title)?$title:'BarrioDescuentos' ?></title>
        <? $description = !empty($product->descripcion)?$product->descripcion:''; ?>
        <!-- SEO -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Language" content="es" />
        <meta name="category" content="news" />
        <meta name="country" content="Spain" />
        <meta name="author" content="Europa Press" />
        <base href="<?= site_url() ?>" />
        <meta name="title" content="<?= !empty($title)?$title:'BarrioDescuentos' ?>" />
        <meta name="description" content="<?= $description ?>" />
        <meta name="og:description" content="<?= $description ?>" />
        <? if(!empty($pic)): ?>
        <link rel="img_src" href="<?= base_url('assets/uploads/'.$pic->row()->url) ?>" />
        <meta property="og:image" content="<?= base_url('assets/uploads/'.$pic->row()->url) ?>" />
        <? endif ?>
        <meta name="date" content="<?= date("Y-m-d"); ?>" />
        <meta name="DC.title" content="<?= !empty($title)?$title:'BarrioDescuentos' ?>" />
        <meta name="DC.date.issued" content="<?= date("Y-m-d")."T".date("H:i:s")."Z" ?>" />
        <meta name="DC.description" content="<?= $description ?>" />
        <meta name="DC.subject" content="<?= !empty($title)?$title:'BarrioDescuentos' ?>" />
        <meta name="DC.creator" content="Barrio descuentos" />
        <meta name="DC.publisher" content="Barrio descuentos" />
        <meta name="DC.language" content="(SCHEME=ISO639) es" />
        <meta name="resource-type" content="document" />
        <meta name="medium" content="news" />
        <meta name="news_keywords" content="<?= !empty($title)?str_replace(' ',',',$title):'BarrioDescuentos' ?>" />
        <link rel="canonical" href="<?= site_url($_SERVER['REQUEST_URI']) ?>" />
        <meta property="og:title" content="<?= !empty($title)?$title:'BarrioDescuentos' ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:site_name" content="barriodescuentos.com" />
        <meta property="og:url" content="<?= site_url($_SERVER['REQUEST_URI']) ?>" />
        <meta name="article:published_time" content="<?= date("Y-m-d")."T".date("H:i:s")."Z" ?>" />
        <link rel="alternate" media="handheld" href="<?= site_url($_SERVER['REQUEST_URI']) ?>" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@barriodescuentos" />
        <meta name="twitter:url" content="<?= site_url($_SERVER['REQUEST_URI']) ?>" />
        <meta name="twitter:title" content="<?= !empty($title)?$title:'BarrioDescuentos' ?>" />
        <meta name="twitter:description" content="<?= $description ?>" />
        <? if(!empty($pic)): ?>
        <meta name="twitter:image" content="<?= base_url('assets/uploads/'.$pic->row()->url) ?>" />
        <? endif ?>
        <meta name="googlebot" content="index,follow,noodp" />
        <meta name="robots" content="index,follow,noodp" />
		
		
		<script src="<?= base_url('assets/grocery_crud/js/jquery-1.8.2.js') ?>"></script>
		<script src="<?= base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
		<script src="<?= base_url('assets/frame.js') ?>"></script>
		<link href="<?= base_url('assets/bootstrap/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
		<link href="<?= base_url('style.css') ?>" rel="stylesheet" type="text/css">
</head>
<body>
	<? $this->load->view('header') ?>	
	<section class="wrap">
		<?php $this->load->view($view) ?>
	</section>
	<footer style="color:white">
    <div>
    <h4>Cuenta</h4>
    <a href="<?= site_url('conectar') ?>">Acceder</a><br/>
    <a href="<?= site_url('registrar') ?>">Crear Cuenta</a><br/>
    <a href="<?= site_url('registrar/forget') ?>">¿Olvido su contraseña?</a>
    </div>

    <div>
    <h4>Sobre Nosotros</h4>
    <a href="<?= site_url('inicio/index/quienes-somos') ?>">Quienes somos</a><br/>
    <a href="<?= site_url('notice') ?>">Noticias</a><br/>
    <a href="<?= site_url('inicio/index/contactenos') ?>">Contacto</a><br/>
    <a href="<?= site_url('inicio/index/nuestro-proyecto-en-think-big') ?>">Nuestro proyecto en Think Big</a>
    </div>

    <div>
    <h4>Saber más</h4>
    <a href="<?= site_url('inicio/index/como-funciona') ?>">Como funciona</a><br/>
    <a href="<?= site_url('inicio/index/terminos-y-condiciones') ?>">Terminos y condiciones</a><br/>
    <a href="<?= site_url('inicio/index/preguntas-frecuentes') ?>">Preguntas frecuentes</a><br/>
    </div>			

    <div>
    <h4>Nuestras Redes</h4>
		<a href="http://www.facebook.com/BarrioDescuentos"><img src="<?= base_url('img/facebook.png') ?>" width="20" style="margin-right:10px"></a>
		<a href="http://www.twitter.com/barriodescuento"><img src="<?= base_url('img/twitter.png') ?>"  width="20"></a>
    </div>
	<div style="display:block"></div>
	<div><a href="<?= site_url() ?>"><img src="<?= base_url('img/logo.png') ?>" height="60"></a></div>
	<div><a href="http://www.thinkbigjovenes.com"><img src="<?= base_url('img/thinkbig.png') ?>"  style="height:80px; margin-left:80px"></a></div>
	<div><a href="http://www.fundacion.telefonica.com"><img src="<?= base_url('img/telefononia.png') ?>" style="height:80px; margin-left:80px;"></a></div>
	<div style="display:block"></div>
	BarrioDescuentos <?= date("Y") ?>. Todos los derechos reservados
</footer>
<script src="<?= base_url('assets/modernizr.js') ?>"></script>
</body>
</html>	
