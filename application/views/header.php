<header class="navbar navbar-inverse">
        <div class="navbar-inner">
            <a class="brand" style="float:left" href="<?= base_url() ?>"><img src="<?= base_url('img/Logo_beta.png') ?>" width="147" /></a>
            <ul class="nav" style="margin-top:20px"> 
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-flag"></i> Localidades <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <? $ciudad = $this->db->get_where('ciudades',array('visible'=>'1')) ?>
                    <? foreach($ciudad->result() as $c): ?>
                        <li><a href="<?= base_url('inicio/ciudad/'.$c->nombre) ?>"><?= $c->nombre ?></a></li>
                    <? endforeach ?>
                </ul>
                </li>
            </ul>    
            <ul class="nav pull-right" style="margin-top:20px">
                <? if(!empty($_SESSION['nombre'])): ?>
                <li class="dropdown">
                <a id="hover" href="<?= base_url('panel'); ?>" ><i class="icon-white icon-user"></i> <?= $_SESSION['nombre'] ?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                   <?= $this->load->view('panel_menu') ?>
                </ul>
                </li>
                <? endif ?>
                
            </ul>
        </div>
	</header>
