<!DOCTYPE html>
<html lang="es">
<head>
	<title><?= !empty($title)?$title:'BarrioDescuentos' ?></title>
        <? $description = !empty($product->descripcion)?$product->descripcion:''; ?>
        <!-- SEO -->
        <link rel="shortcut icon" href="<?= base_url('img/favicon.ico') ?>">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Language" content="es" />
        <meta name="category" content="news" />
        <meta name="country" content="Spain" />
        <meta name="author" content="Europa Press" />
        <base href="<?= site_url() ?>" />
        <meta name="title" content="<?= !empty($title)?$title:'BarrioDescuentos' ?>" />
        <meta name="description" content="<?= $description ?>" />
        <meta name="og:description" content="<?= $description ?>" />
        <? if(!empty($pic)): ?>
        <link rel="img_src" href="<?= base_url('assets/uploads/'.$pic->row()->url) ?>" />
        <meta property="og:image" content="<?= base_url('assets/uploads/'.$pic->row()->url) ?>" />
        <? endif ?>
        <meta name="date" content="<?= date("Y-m-d"); ?>" />
        <meta name="DC.title" content="<?= !empty($title)?$title:'BarrioDescuentos' ?>" />
        <meta name="DC.date.issued" content="<?= date("Y-m-d")."T".date("H:i:s")."Z" ?>" />
        <meta name="DC.description" content="<?= $description ?>" />
        <meta name="DC.subject" content="<?= !empty($title)?$title:'BarrioDescuentos' ?>" />
        <meta name="DC.creator" content="Barrio descuentos" />
        <meta name="DC.publisher" content="Barrio descuentos" />
        <meta name="DC.language" content="(SCHEME=ISO639) es" />
        <meta name="resource-type" content="document" />
        <meta name="medium" content="news" />
        <meta name="news_keywords" content="<?= !empty($title)?str_replace(' ',',',$title):'BarrioDescuentos' ?>" />
        <link rel="canonical" href="<?= site_url($_SERVER['REQUEST_URI']) ?>" />
        <meta property="og:title" content="<?= !empty($title)?$title:'BarrioDescuentos' ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:site_name" content="barriodescuentos.com" />
        <meta property="og:url" content="<?= site_url($_SERVER['REQUEST_URI']) ?>" />
        <meta name="article:published_time" content="<?= date("Y-m-d")."T".date("H:i:s")."Z" ?>" />
        <link rel="alternate" media="handheld" href="<?= site_url($_SERVER['REQUEST_URI']) ?>" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@barriodescuentos" />
        <meta name="twitter:url" content="<?= site_url($_SERVER['REQUEST_URI']) ?>" />
        <meta name="twitter:title" content="<?= !empty($title)?$title:'BarrioDescuentos' ?>" />
        <meta name="twitter:description" content="<?= $description ?>" />
        <? if(!empty($pic)): ?>
        <meta name="twitter:image" content="<?= base_url('assets/uploads/'.$pic->row()->url) ?>" />
        <? endif ?>
        <meta name="googlebot" content="index,follow,noodp" />
        <meta name="robots" content="index,follow,noodp" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		
	<script src="<?= base_url('assets/grocery_crud/js/jquery-1.8.2.js') ?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
	<script src="<?= base_url('assets/frame.js') ?>"></script>
	<link href="<?= base_url('assets/bootstrap/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('assets/bootstrap/css/bootstrap-responsive.min.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('style.css') ?>" rel="stylesheet" type="text/css">
        
        <!-- MEGAFOLIO PRO GALLERY CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('css/settings.css') ?>" media="screen" />
        <!-- MEGAFOLIO PRO GALLERY JS FILES  -->
	<script type="text/javascript" src="<?= base_url('js/jquery.themepunch.plugins.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('js/jquery.themepunch.megafoliopro.js') ?>"></script>
        <!-- THE FANYCYBOX ASSETS -->
	<link rel="stylesheet" href="<?= base_url('css/jquery.fancybox.css?v=2.1.3') ?>" type="text/css" media="screen" />
	<script type="text/javascript" src="<?= base_url('js/jquery.fancybox.pack.js?v=2.1.3') ?>"></script>
</head>
<body>
	<? $this->load->view('header') ?>	
	<section>
		<?php $this->load->view($view) ?>
	</section>
        <? $this->load->view('footer') ?>
        <script src="<?= base_url('assets/modernizr.js') ?>"></script>
        <script type="text/javascript">
        jQuery(document).ready(function() {
        var api=jQuery('.megafolio-container').megafoliopro(
        {
	filterChangeAnimation:"fade",	// fade, rotate, scale, rotatescale, pagetop, pagebottom,pagemiddle
	filterChangeSpeed:800,					// Speed of Transition
	filterChangeRotate:99,					// If you ue scalerotate or rotate you can set the rotation (99 = random !!)
	filterChangeScale:0.6,					// Scale Animation Endparameter
	delay:20,								// The Time between the Animation of single mega-entry points
        paddingHorizontal:0,					// Padding between the mega-entrypoints
	paddingVertical:0,
	layoutarray:[12]		// Defines the Layout Types which can be used in the Gallery. 2-9 or "random". You can define more than one, like {5,2,6,4} where the first items will be orderd in layout 5, the next comming items in layout 2, the next comming items in layout 6 etc... You can use also simple {9} then all item ordered in Layout 9 type.
        });

            // CALL FILTER FUNCTION IF ANY FILTER HAS BEEN CLICKED
        jQuery('.filter').click(function() {	api.megafilter(jQuery(this).data('category'));    });
        // THE FANCYBOX PLUGIN INITALISATION
        jQuery(".fancybox").fancybox();
        });
        
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46401097-1', 'barriodescuentos.es');
  ga('send', 'pageview');
</script>
</body>
</html>	
