<!DOCTYPE html>
<html lang="en">
<head>
	<title>Panel de control</title>
	<meta charset="utf-8">
        <link rel="shortcut icon" href="<?= base_url('img/favicon.ico') ?>">
        <script src="<?= base_url('assets/grocery_crud/js/jquery-1.8.2.js') ?>"></script>
	<script src="<?= base_url('assets/bootstrap/js/bootstrap.js') ?>"></script>
	<script src="<?= base_url('assets/frame.js') ?>"></script>

	<link href="<?= base_url('assets/bootstrap/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
         <link href="<?= base_url('assets/bootstrap/css/bootstrap-responsive.min.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('assets/bootstrap/css/docs.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('style.css') ?>" rel="stylesheet" type="text/css">
        
        <!-- MEGAFOLIO PRO GALLERY CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('css/settings.css') ?>" media="screen" />
        <!-- MEGAFOLIO PRO GALLERY JS FILES  -->
	<script type="text/javascript" src="<?= base_url('js/jquery.themepunch.plugins.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('js/jquery.themepunch.megafoliopro.js') ?>"></script>
        <!-- THE FANYCYBOX ASSETS -->
	<link rel="stylesheet" href="<?= base_url('css/jquery.fancybox.css?v=2.1.3') ?>" type="text/css" media="screen" />
	<script type="text/javascript" src="<?= base_url('js/jquery.fancybox.pack.js?v=2.1.3') ?>"></script>
        
	<?php 
	foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
	<?php endforeach; ?>
	<?php foreach($js_files as $file): ?>
	<script src="<?= $file ?>"></script>
	<?php endforeach; ?>
</head>
<body style="padding-top:0px;">
	<? $this->load->view('header') ?>
	<section style="min-height: 547px; padding: 30px 30px;">
               <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span3">
                        <ul class="nav nav-pills nav-stacked  bs-docs-sidenav" style="text-align:left;">
                        <?= $this->load->view('panel_menu') ?>
                        </ul>
                    </div>
                <div class="span9">
                    <?php $this->load->view($view,array('output'=>$output,'ventas'=>$this->main->getVentas())) ?>
                </div>
                </div>
               </div>
	</section>
        <? $this->load->view('footer') ?>
        <? 
        $notice = $this->main->getNoticeReg(); 
        $noticia = '';
        foreach($notice->result() as $n): 
        $vistas = explode(",",$n->vistas);
        if(!in_array($_SESSION['user'],$vistas)){
        $noticia .= "<b>".$n->titulo."</b><p>".trim(strip_tags($n->texto))."</p>";
        array_push($vistas,$_SESSION['user']);
        $this->db->where('id',$n->id);
        $this->db->update('noticias',array('vistas'=>implode(',',$vistas)));
        }
        ?>
        <? endforeach ?>
        <? if(!empty($noticia)): ?>
        <script>emergente('<?= $noticia ?>')</script>
        <? endif ?>
</body>
</html>	