<footer class="navbar-inverse">
    <div class="row-fluid">
    <div class="span3">
    <h4>Cuenta</h4>
    <a href="<?= site_url('conectar') ?>">Acceder</a><br/>
    <a href="<?= site_url('registrar') ?>">Crear Cuenta</a><br/>
    <a href="<?= site_url('registrar/forget') ?>">¿Olvidó su contraseña?</a>
    </div>

    <div class="span3">
    <h4>Sobre Nosotros</h4>
    <a href="<?= site_url('pages/'.date("Y").'/quienes-somos') ?>">¿Quiénes somos?</a><br/>
    <a href="<?= site_url('notice') ?>">Noticias</a><br/>
    <a href="<?= site_url('pages/'.date("Y").'/contactenos') ?>">Contáctenos</a><br/>
    <a href="<?= site_url('pages/'.date("Y").'/nuestro-proyecto-en-think-big') ?>">Nuestro proyecto en Think Big</a>
    </div>

    <div class="span3">
    <h4>Saber más</h4>
    <a href="<?= site_url('pages/'.date("Y").'/como-funciona') ?>">¿Cómo funciona?</a><br/>
    <a href="<?= site_url('pages/'.date("Y").'/terminos-y-condiciones') ?>">Términos y condiciones</a><br/>
    <a href="<?= site_url('pages/'.date("Y").'/preguntas-frecuentes') ?>">Preguntas frecuentes</a><br/>
    </div>			

    <div class="span3">
    <h4>Nuestras Redes</h4>
		<a href="http://www.facebook.com/BarrioDescuentos"><img src="<?= base_url('img/facebook.png') ?>" width="32" style="margin-right:10px"></a>
		<a href="http://www.twitter.com/barriodescuento"><img src="<?= base_url('img/twitter.png') ?>"  width="32"></a><br/>
                <a href="https://plus.google.com/105360073810746525937" rel="publisher"><img src="https://www.gstatic.com/images/icons/gplus-32.png" alt="Compartir en Google+"/></a>
                <a href="https://www.youtube.com/user/BarrioDescuentos"><img src="<?= base_url('img/youtube.jpg') ?>" width="32" style="margin-right:10px"></a>
    </div>
    </div>
    <div class="row-fluid">
	<div class="span4"  style="padding:30px 0"><a href="<?= site_url() ?>"><img src="<?= base_url('img/Logo_beta.png') ?>" width="147" /></a></div>
	<div class="offset1 span2"><a href="http://www.thinkbigjovenes.com"><img src="<?= base_url('img/thinkbig.png') ?>" ></a></div>
	<div class="offset2 span2"><a href="http://www.fundacion.telefonica.com"><img src="<?= base_url('img/telefononia.png') ?>"></a></div>
    </div>
    <div class="row-fluid">
        <div class="span12">BarrioDescuentos <?= date("Y") ?>. Todos los derechos reservados</div>
    </div>
</footer>